﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int coinValue = 0;
    public int diamondValue = 0;
    public int foodValue = 0;
    public int totalStars = 0;
    public int unlockLevelValue = 1;
    public float musicVolumeValue = 0;
    public float soundEffectsVolumeValue = 0;
    public bool unlockAllNormalLevelValue = false;
    public bool isShowedUnlockHardLevel = false;
    public bool ReplayTeachValue = false;
    public bool ReplayStartStoryValue = false;
    public int[] crystalsValue = new int[10];
    public int[] levelStarsValue = new int[10];
    public bool[] achievement = new bool[10];
    public bool[] achievementTaken = new bool[10];
    public bool isBuyBeard = false;
    public bool isBuyMalay = false;
    public int clothesNumber = 0;
}
