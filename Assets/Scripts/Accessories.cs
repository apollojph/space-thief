﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accessories : MonoBehaviour 
{

    public GameObject hat;
    public GameObject main;
    public IntVariable totalStars;

	void Start () 
    {
        Invoke("Show", 0.1f);
	}

    private void Show()
    {
        if (totalStars.Value == 30)
        {
            hat.SetActive(true);
            main.SetActive(true);
        }
    }
}
