﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{

    public FloatVariable playedTime;

    void Update ()
    {
        if (playedTime.Value <= 60)
        {
            playedTime.ApplyChange(Time.deltaTime);
        }
    }
}
