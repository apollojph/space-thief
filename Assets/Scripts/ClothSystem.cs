﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothSystem : MonoBehaviour 
{
    public Texture clothTexture;

    private Renderer meshRenderer;

	void Awake () 
    {
        meshRenderer = transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>();
	}

    public void ChangeCloth(int number)
    {
        meshRenderer.material.SetTexture("_MainTex", clothTexture);
    }
}
