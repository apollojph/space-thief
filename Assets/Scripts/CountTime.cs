﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CountTime : MonoBehaviour {

	//GUIStyle myStyle;
	public Text timerLabel;
	private string timerText;
	private int roundSeconds;

	private int txtSeconds;
	private int txtMinutes;

	public static float timer;

	void Awake () 
	{

	}

	void Start () 
	{
		timer = 0;


	}
	

	void Update ()
	{
		if (Input.GetKeyUp(KeyCode.T)) {

			timer += 30;
		}
		timer = timer + Time.deltaTime;
		if (timer <= 0) 
		{
			timer = 0;
		}

		roundSeconds = Mathf.CeilToInt (timer);

		txtSeconds = roundSeconds % 60;
		txtMinutes = roundSeconds / 60;

		timerText = string.Format ("{0:00}:{1:00}",txtMinutes,txtSeconds);
		timerLabel.text = timerText;

	}
	public void buytime_time(int price){

		if (timer <= 170 && GameManager.Instance.Coin > 1000) 
		{
			timer += 10;
			GameManager.Instance.Coin -= price;
		}

	}

}
