﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmojiSystem : MonoBehaviour
{
    public float changeTime = 3f;
    public Vector3 offset;
    public GameObject[] emojiSources;
    public bool isChanging = false;
    public GameObject emojiInstance;

    private PlayerController player;


    private void Awake()
    {
        player = GetComponent<PlayerController> ();
    }

    private void Update()
    {
        if (player.state == PlayerController.State.Moving && !isChanging)
        {
            StartCoroutine(ChangeNormalEmoji());
        }
    }

    public IEnumerator ChangeNormalEmoji()
    {
        isChanging = true;

        yield return new WaitForSeconds(changeTime);

        isChanging = false;

        int emojiNumber = GetRandomEmoji();

        if(!emojiSources[emojiNumber].activeSelf)
        {
            ChangeEmoji(emojiNumber);
        }
    }

    public void ChangeEmoji(int emojiNumber)
    {
        //Close all emoji

        foreach(GameObject emoji in emojiSources)
        {
            emoji.SetActive(false);
        }

        //To open the needed emoji
        emojiSources[emojiNumber].SetActive(true);
    }

    private int GetRandomEmoji()
    {
        int value = Random.Range(0, 2);
        return value;
    }

    public void Hide()
    {
        emojiInstance.SetActive(false);

        foreach (GameObject emoji in emojiSources)
        {
            emoji.SetActive(false);
        }

        enabled = false;
    }

    public void Show()
    {
        emojiInstance.SetActive(true);

        enabled = true;
    }
}
