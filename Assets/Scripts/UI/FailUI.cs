﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FailUI : UISystem 
{
    public Image failStatusUI;
    public Image tipUI;
    public Sprite[] failStatusSprites;
    public Sprite[] tipSprites;

    public override void Initialize()
    {
        Hide();
	}

    public void SetFailStatus(int number)
    {
        failStatusUI.sprite = failStatusSprites[number];
    }

    public void SetTip()
    {
        int number = UnityEngine.Random.Range(0, 5);

        tipUI.sprite = tipSprites[number];

        print("SetTip");
    }
}
