﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class UfoMenu : MonoBehaviour
{

	public GameObject UFOMenu;
	public GameObject UFOMenu_Button;
	public GameObject PetMenu;
	public GameObject FeedMenu;
	public GameObject SettingMenu;
	public GameObject MixMenu;
	public GameObject NoteBoardMenu;
	public GameObject Button_Setting;
	public GameObject Button_Mix;
	public GameObject Button_Notebook;
	public GameObject Button_Pet;
	public GameObject planet_Teach_4;
	public GameObject planet_Teach_5;
	public GameObject planet_Teach_menu;
	private int page = 0;
	public GameObject Button_NextPage;
	public GameObject Button_PreviousPage;
	public GameObject NoteBoardPage_0;
	public GameObject NoteBoardPage_1;
	public GameObject NoteBoardPage_2;
	public GameObject NoteBoardPage_3;
	public GameObject AchievementMenu;
	public GameObject ClothMenu;

	public GameObject Growupvalue10;
	public GameObject Growupvalue20;
	public GameObject Growupvalue30;
	public GameObject Growupvalue40;
	public GameObject Growupvalue50;

	public static bool eat_check = false;
	public static bool eat_normal = false;
	public static bool eat_advanced = false;
	public static bool backfood = false;
	public static bool sleep = false;
	public static bool pet = false;
	public static bool eatfinish = true;
	public static bool Achievement = false;
	public static bool Clothes = false;

	public GameObject Eating_normal;
	public GameObject Cover_normal;
	public GameObject Eating_advanced;
	public GameObject Cover_advanced;
	public GameObject Sleeping;
	public GameObject Walking_Right;
	public GameObject Walking_Left;
	public GameObject PetWalk_Right;
	public GameObject PetWalk_Left;
	public GameObject Poking_right;
	public GameObject Poking_left;

	public Animator pillow;
	public Text timerLabel_right;
	public Text timerLabel_left;
	private string timerText;
	private int roundSeconds;

	private int txtSeconds;
	private int txtMinutes;
	public Image Money_Right;
	public Image Money_Left;
	public Sprite[] Money_RightSprites;
	public Sprite[] Money_LeftSprites;
	public GameObject Pick_Right;
	public GameObject Pick_Left;
	public Button Feed;

	public Text AddMoney;
	private int txtAddMoney;
	public GameObject AddMoneycoin;

    public IntVariable coin;
	public IntVariable diamonds;
	public IntVariable food;
	public IntVariable clothesNumber;
	public IntVariable TotalCrystal;
    public FloatVariable playedTime;
	public BoolVariable ReplayTeach;
	public BoolVariable ReplayStartStory;
    public BoolVariable achievement_1;
	public BoolVariable BuyBeard;
	public BoolVariable BuyMalay;

	public Text coinsText;
	public Text TotalCrystalText;
	public Text coinsText_cloth;
	public Text diamondsText_cloth;

    public Button pickMoneyButtonLeft;
    public Button pickMoneyButtonRight;

    public GameObject[] petBuffs = new GameObject[3];
	public GameObject[] ClothChange = new GameObject[4];
	public GameObject[] ClothChange_Previous = new GameObject[4];
	public GameObject[] ClothChangeButton = new GameObject[4];
	public GameObject[] ClothUseing = new GameObject[4];
	public GameObject BuyBeardButton;
	public GameObject BuyMalayButton;
    public GameObject food_30_text;
    public GameObject questionmark_01;
    public GameObject questionmark_02;

    void Start () 
	{		
		/*planet_Teach_4.SetActive (false);
		planet_Teach_5.SetActive (false);
		planet_Teach_menu.SetActive (false);
		StartCoroutine(Button_UFOMenu(1.3f));*/
		if (chossui.planet_Teach04 == true) 
		{
			planet_Teach_4.SetActive (true);
			planet_Teach_menu.SetActive (true);
			StartCoroutine(planet_Teach_04(1.2f));
		}
		else if (chossui.planet_Teach04 == false) 
		{
			planet_Teach_4.SetActive (false);
			planet_Teach_5.SetActive (false);
			planet_Teach_menu.SetActive (false);
			StartCoroutine(Button_UFOMenu(1.2f));
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
			
		if (playedTime.Value >= 60) 
		{
			Pick_Right.SetActive (true);
			Pick_Left.SetActive (true);
            Money_RightStatus(4);
            Money_LeftStatus(4);
		}

		if (playedTime.Value < 55 && playedTime.Value > 40) 
		{
			Money_RightStatus(4);
			Money_LeftStatus(4);

		}
		if (playedTime.Value < 40 && playedTime.Value > 30) 
		{
			Money_RightStatus(3);
			Money_LeftStatus(3);

		}
		if (playedTime.Value < 30 && playedTime.Value > 20) 
		{
			Money_RightStatus(2);
			Money_LeftStatus(2);
		}
		if (playedTime.Value < 20 && playedTime.Value > 10) 
		{
			Money_RightStatus(1);
			Money_LeftStatus(1);
		}
		if (playedTime.Value >= 0 && playedTime.Value < 10) 
		{
			Money_RightStatus(0);
			Money_LeftStatus(0);

		}

		roundSeconds = Mathf.CeilToInt (playedTime.Value);

		txtSeconds = roundSeconds % 60;
		txtMinutes = roundSeconds / 60;

		timerText = string.Format ("{0:00}:{1:00}",txtMinutes,txtSeconds);
		timerLabel_right.text = timerText;
		timerLabel_left.text = timerText;


		if (food.Value >= 10) 
		{
			Growupvalue10.SetActive (true);
            petBuffs[0].SetActive(true);
		}
		if (food.Value >= 20) 
		{
			Growupvalue20.SetActive (true);
		}
		if (food.Value >= 30) 
		{
			Growupvalue30.SetActive (true);
            petBuffs[1].SetActive(true);
            food_30_text.SetActive(false);
        }
		if (food.Value >= 40) 
		{
			Growupvalue40.SetActive (true);
		}
		if (food.Value >= 50) 
		{
			Growupvalue50.SetActive (true);
            petBuffs[2].SetActive(true);
		}        

        if (eat_normal == true && backfood == true)
		{
			eatfinish = false;
			eat_normal = false;
			Cover_normal.SetActive (false);
			Eating_normal.SetActive (true);
			Eating_advanced.SetActive (false);
			Sleeping.SetActive (false);
			Walking_Right.SetActive (false);
			Walking_Left.SetActive (false);
			StartCoroutine(Walk_Right_eatfinish(0.1f));
		}
		if(eat_advanced == true && backfood == true)
		{
			eatfinish = false;
			eat_advanced = false;
			Cover_advanced.SetActive (false);
			Eating_advanced.SetActive (true);
			Eating_normal.SetActive (false);
			Sleeping.SetActive (false);
			Walking_Right.SetActive (false);
			Walking_Left.SetActive (false);
			StartCoroutine(Walk_Right_eatfinish(0.1f));
		}
		if (clothesNumber.Value == 0) 
		{
			ClothChange [0].SetActive (true);
			ClothChange_Previous [0].SetActive (true);
			ClothChange [1].SetActive (false);
			ClothChange_Previous [1].SetActive (false);
			ClothChange [2].SetActive (false);
			ClothChange_Previous [2].SetActive (false);
			ClothChange [3].SetActive (false);
			ClothChange_Previous [3].SetActive (false);
			ClothUseing[0].SetActive (true);
			ClothUseing[1].SetActive (false);
			ClothUseing[2].SetActive (false);
			ClothUseing[3].SetActive (false);
			ClothChangeButton[0].SetActive (false);

            if (food.Value >= 30)
            {
                ClothChangeButton[1].SetActive(true);
            }

            if (BuyBeard.Value==true) 
			{
				BuyBeardButton.SetActive (false);
				ClothChangeButton[2].SetActive (true);
			}

			if (BuyMalay.Value==true) 
			{
				BuyMalayButton.SetActive (false);
				ClothChangeButton[3].SetActive (true);
			}
		}
       
		if (clothesNumber.Value == 1) 
		{
			ClothChange [0].SetActive (false);
			ClothChange_Previous [0].SetActive (false);
			ClothChange [1].SetActive (true);
			ClothChange_Previous [1].SetActive (true);
			ClothChange [2].SetActive (false);
			ClothChange_Previous [2].SetActive (false);
			ClothChange [3].SetActive (false);
			ClothChange_Previous [3].SetActive (false);
			ClothUseing[0].SetActive (false);
			ClothUseing[1].SetActive (true);
			ClothUseing[2].SetActive (false);
			ClothUseing[3].SetActive (false);
			ClothChangeButton[0].SetActive (true);
			ClothChangeButton[1].SetActive (false);

			if (BuyBeard.Value==true) 
			{
				BuyBeardButton.SetActive (false);
				ClothChangeButton[2].SetActive (true);
			}
			if (BuyMalay.Value==true) 
			{
				BuyMalayButton.SetActive (false);
				ClothChangeButton[3].SetActive (true);
			}
		}
		if (clothesNumber.Value == 2) 
		{
			ClothChange [0].SetActive (false);
			ClothChange_Previous [0].SetActive (false);
			ClothChange [1].SetActive (false);
			ClothChange_Previous [1].SetActive (false);
			ClothChange [2].SetActive (true);
			ClothChange_Previous [2].SetActive (true);
			ClothChange [3].SetActive (false);
			ClothChange_Previous [3].SetActive (false);
			ClothUseing[0].SetActive (false);
			ClothUseing[1].SetActive (false);
			ClothUseing[2].SetActive (true);
			ClothUseing[3].SetActive (false);
			ClothChangeButton[0].SetActive (true);
			ClothChangeButton[2].SetActive (false);

            if (food.Value >= 30)
            {
                ClothChangeButton[1].SetActive(true);
            }
            if (BuyMalay.Value==true) 
			{
				BuyMalayButton.SetActive (false);
				ClothChangeButton[3].SetActive (true);
			}
		}
		if (clothesNumber.Value == 3) 
		{
			ClothChange [0].SetActive (false);
			ClothChange_Previous [0].SetActive (false);
			ClothChange [1].SetActive (false);
			ClothChange_Previous [1].SetActive (false);
			ClothChange [2].SetActive (false);
			ClothChange_Previous [2].SetActive (false);
			ClothChange [3].SetActive (true);
			ClothChange_Previous [3].SetActive (true);
			ClothUseing[0].SetActive (false);
			ClothUseing[1].SetActive (false);
			ClothUseing[2].SetActive (false);
			ClothUseing[3].SetActive (true);
			ClothChangeButton[0].SetActive (true);
			ClothChangeButton[3].SetActive (false);

            if (food.Value >= 30)
            {
                ClothChangeButton[1].SetActive(true);
            }
            if (BuyBeard.Value==true) 
			{
				BuyBeardButton.SetActive (false);
				ClothChangeButton[2].SetActive (true);
			}
		}
	
	}

	public void OpenFB()
	{
		Application.OpenURL ("https://www.facebook.com/SpaceThief.alpha/");
	}

	public void CloseChoose1()
	{
		if (chossui.planet_Teach04 == false) 
		{
			Achievement = false;
            StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("MainMenu"));
            UnityEngine.Object.FindObjectOfType<DataSystem>().SaveData();
		}
	}
/***************************************************************************/
	public void OpenSetting()
	{
		pet = false;
		PetMenu.SetActive (false);
		SettingMenu.SetActive (true);
		NoteBoardMenu.SetActive (false);
		MixMenu.SetActive (false);

		UFOMenu.SetActive (false);

		Button_Pet.SetActive (true);
		Button_Setting.SetActive (false);
		Button_Notebook.SetActive (true);
		Button_Mix.SetActive (true);
	}



/***************************************************************************/

	public void OpenMix()
	{
		pet = false;
		PetMenu.SetActive (false);
		MixMenu.SetActive (true);
		NoteBoardMenu.SetActive (false);
		SettingMenu.SetActive (false);

		UFOMenu.SetActive (false);

		Button_Pet.SetActive (true);
		Button_Mix.SetActive (false);
		Button_Setting.SetActive (true);
		Button_Notebook.SetActive (true);

		coinsText.text = coin.Value.ToString();
		TotalCrystalText.text = TotalCrystal.Value.ToString();
	}
    public void OpenQuestionInMix()
    {
        questionmark_02.SetActive(true);
    }
    public void CloseQuestionInMix()
    {
        questionmark_02.SetActive(false);
    }

    /***************************************************************************/

    public void OpenPet()
	{
		pet = true;
		PetMenu.SetActive (true);
		MixMenu.SetActive (false);
		NoteBoardMenu.SetActive (false);
		SettingMenu.SetActive (false);

		UFOMenu.SetActive (false);

		Button_Pet.SetActive (false);
		Button_Mix.SetActive (true);
		Button_Setting.SetActive (true);
		Button_Notebook.SetActive (true);
		//StartCoroutine(Walk_Right(7.0f));

	}
	private void Money_RightStatus(int number)
	{
		Money_Right.sprite = Money_RightSprites[number];
	}
	private void Money_LeftStatus(int number)
	{
		Money_Left.sprite = Money_LeftSprites[number];
	}
	public void PickMoney_right()
	{
        Pick_Right.SetActive(false);
		AddMoneycoin.SetActive (true);
		txtAddMoney = 20 + food.Value * 2;
        coin.ApplyChange(txtAddMoney);
		AddMoney.text ="+ "+ txtAddMoney.ToString();
		Feed.interactable = true;
		PetWalk_Right.SetActive (false);
		Poking_right.SetActive (true);
        playedTime.ApplyChange(-60);
		StartCoroutine(Walk_Left(1.5f));
	}
	public void PickMoney_Left()
	{
        Pick_Left.SetActive(false);
		AddMoneycoin.SetActive (true);
		txtAddMoney = 20 + food.Value * 2;
        coin.ApplyChange(txtAddMoney);
        AddMoney.text ="+ "+ txtAddMoney.ToString();
		Feed.interactable = true;
		PetWalk_Left.SetActive (false);
		Poking_left.SetActive (true);
        playedTime.ApplyChange(-60);
        StartCoroutine(Walk_Right(1.5f));
	}
	public void Pickpet_right()
	{
		PetWalk_Right.SetActive (false);
		Poking_right.SetActive (true);
		StartCoroutine(Walk_Left(1.5f));
	}
	public void Pickpet_Left()
	{
		PetWalk_Left.SetActive (false);
		Poking_left.SetActive (true);
		StartCoroutine(Walk_Right(1.5f));
	}
	public void OpenFeed()
	{
		FeedMenu.SetActive (true);
		eat_check = false;
		backfood = false;
	}
	public void CloseFeed()
	{
		FeedMenu.SetActive (false);
		backfood = true;
	}

	public void RecordTime(int moneytime)
	{
		if(playedTime.Value <= 0)
		{
            playedTime.ApplyChange(moneytime);
		}

	}

	public void Eatnormal()
	{
		if (eat_check == true) 
		{
			eat_normal = true;
			eat_check = false;
			sleep = false;
		}

	}
	public void Eatadvanced()
	{
		if (eat_check == true) 
		{
			eat_advanced = true;
			eat_normal = false;
			eat_check = false;
			sleep = false;
		}
			
	}
	public void Pillow()
	{ 
		pillow.speed = 0;
		sleep = true;
		Walking_Left.SetActive (false);
		Walking_Right.SetActive (false);
		Cover_normal.SetActive (true);
		Cover_advanced.SetActive (true);
		Eating_normal.SetActive (false);
		Eating_advanced.SetActive (false);
		Sleeping.SetActive (true);
	}

	public void WalkRight()
	{
		if (sleep == false) 
		{
			StartCoroutine (Walk_Right (0.2f));
		}
		if (sleep == true) 
		{
			pillow.speed = 1;
			sleep = false;
			Poking_right.SetActive (false);
			Cover_normal.SetActive (true);
			Cover_advanced.SetActive (true);
			Eating_normal.SetActive (false);
			Eating_advanced.SetActive (false);
			Sleeping.SetActive (false);
			StartCoroutine(Walk_Right(0.2f));
		}
	}
	public void WalkLeft()
	{
		if (sleep == false) 
		{
			StartCoroutine(Walk_Left(0.2f));
		}
		if (sleep == true) 
		{
			pillow.speed = 1;
			sleep = false;
			Poking_left.SetActive (false);
			Cover_normal.SetActive (true);
			Cover_advanced.SetActive (true);
			Eating_normal.SetActive (false);
			Eating_advanced.SetActive (false);
			Sleeping.SetActive (false);
			StartCoroutine(Walk_Right(0.2f));
		}

	}
    public void OpenQuestionInPet()
    {
        questionmark_01.SetActive(true);
    }
    public void CloseQuestionInPet()
    {
        questionmark_01.SetActive(false);
    }

    IEnumerator Walk_Right_eatfinish(float time)
	{
		yield return new WaitForSeconds(time);
		eatfinish = true;
		StartCoroutine(Walk_Right(6.0f));
	}

	IEnumerator Walk_Left(float time)
	{
		yield return new WaitForSeconds(time);

		if (sleep == false && pet==true && eatfinish==true) 
		{
			Cover_normal.SetActive (true);
			Cover_advanced.SetActive (true);
			Eating_normal.SetActive (false);
			Eating_advanced.SetActive (false);
			AddMoneycoin.SetActive (false);
			PetWalk_Left.SetActive (true);
			Pick_Left.SetActive (false);
			Walking_Right.SetActive (false);
			Walking_Left.SetActive (true);
			Poking_right.SetActive (false);
            pickMoneyButtonLeft.interactable = true;
			//StartCoroutine(Walk_Right(7.0f));
		}
	}
	IEnumerator Walk_Right(float time)
	{
		yield return new WaitForSeconds(time);

	
		if (sleep == false && pet==true && eatfinish==true) 
		{
			Cover_normal.SetActive (true);
			Cover_advanced.SetActive (true);
			Eating_normal.SetActive (false);
			Eating_advanced.SetActive (false);
			AddMoneycoin.SetActive (false);
			PetWalk_Right.SetActive (true);
			Pick_Right.SetActive (false);
			Walking_Right.SetActive (true);
			Walking_Left.SetActive (false);
			Poking_left.SetActive (false);
			//StartCoroutine(Walk_Left(7.0f));
		}

	}
/***************************************************************************/


	public void OpenNoteBoard()
	{
		pet = false;
		PetMenu.SetActive (false);
		NoteBoardMenu.SetActive (true);
		SettingMenu.SetActive (false);
		MixMenu.SetActive (false);

		UFOMenu.SetActive (false);

		Button_Pet.SetActive (true);
		Button_Notebook.SetActive (false);
		Button_Mix.SetActive (true);
		Button_Setting.SetActive (true);
		if (Achievement == true||Clothes == true) 
		{
			NoteBoardPage_2.SetActive (true);
			Button_NextPage.SetActive (true);
			AchievementMenu.SetActive (false);
			ClothMenu.SetActive (false);
			Achievement = false;
			Clothes = false;
		}


	}
	public void GoNextPage()
	{
		page += 1;
		if (page == 0) 
		{
			print("page: " + page);
			NoteBoardPage_0.SetActive (true);
			NoteBoardPage_1.SetActive (false);
			NoteBoardPage_2.SetActive (false);
			NoteBoardPage_3.SetActive (false);
			Button_NextPage.SetActive (true);
			Button_PreviousPage.SetActive (false);
		}
		if (page == 1) 
		{
			print("page: " + page);
			NoteBoardPage_0.SetActive (false);
			NoteBoardPage_1.SetActive (true);
			NoteBoardPage_2.SetActive (false);
			NoteBoardPage_3.SetActive (false);
			Button_NextPage.SetActive (false);
			Button_PreviousPage.SetActive (true);
		}

	}
	public void PreviousPage()
	{
		page -= 1;
		if (page == 0) 
		{
			print("page: " + page);
			NoteBoardPage_0.SetActive (false);
			NoteBoardPage_1.SetActive (false);
			NoteBoardPage_2.SetActive (true);
			NoteBoardPage_3.SetActive (false);
			Button_NextPage.SetActive (true);
			Button_PreviousPage.SetActive (false);
		}
		if (page == 1) 
		{
			print("page: " + page);
			NoteBoardPage_0.SetActive (false);
			NoteBoardPage_1.SetActive (false);
			NoteBoardPage_2.SetActive (false);
			NoteBoardPage_3.SetActive (true);
			Button_NextPage.SetActive (false);
			Button_PreviousPage.SetActive (true);
		}

	}

	public void CloseAchievement()
	{
		print("page: " + page);
		NoteBoardPage_0.SetActive (false);
		NoteBoardPage_1.SetActive (false);
		NoteBoardPage_2.SetActive (true);
		NoteBoardPage_3.SetActive (false);
		Button_NextPage.SetActive (true);
		AchievementMenu.SetActive (false);
		Achievement = false;
	}
	public void OpenAchievement()
	{
		Achievement = true;
		NoteBoardPage_0.SetActive (false);
		NoteBoardPage_2.SetActive (false);
		Button_NextPage.SetActive (false);
		AchievementMenu.SetActive (true);
	}
	public void CloseClothes()
	{
		print("page: " + page);
		NoteBoardPage_0.SetActive (false);
		NoteBoardPage_1.SetActive (false);
		NoteBoardPage_2.SetActive (true);
		NoteBoardPage_3.SetActive (false);
		Button_NextPage.SetActive (true);
		ClothMenu.SetActive (false);
		Clothes = false;
	}
	public void OpenClothes()
	{
		coinsText_cloth.text = coin.Value.ToString();
		diamondsText_cloth.text = diamonds.Value.ToString();
		Clothes = true;
		NoteBoardPage_0.SetActive (false);
		NoteBoardPage_2.SetActive (false);
		Button_NextPage.SetActive (false);
		ClothMenu.SetActive (true);

	}
	public void UseClothes01()
	{
		clothesNumber.Value = 0;
	}
	public void UseClothes02()
	{
		clothesNumber.Value = 1;
	}
	public void UseClothes03()
	{
		clothesNumber.Value = 2;
	}
	public void UseClothes04()
	{
		clothesNumber.Value = 3;
	}
	public void Buy_Beard(int value)
	{

		int currentCoin = coin.Value;
		int nextCoin = coin.Value - value;

		if(currentCoin > nextCoin && nextCoin > -1)
		{
			BuyBeard.SetValue (true);
			UfoMenu.eat_check = true;
			coin.ApplyChange(-value);
			BuyBeardButton.SetActive (false);
			ClothChangeButton[2].SetActive (true);
			GetComponent<UIEvent>().SaveData();
		}

	}
	public void Buy_Malay(int value)
	{

		int currentCoin = coin.Value;
		int nextCoin = coin.Value - value;

		if(currentCoin > nextCoin && nextCoin > -1)
		{
			BuyMalay.SetValue (true);
			UfoMenu.eat_check = true;
			coin.ApplyChange(-value);
			BuyMalayButton.SetActive (false);
			ClothChangeButton[3].SetActive (true);
			GetComponent<UIEvent>().SaveData();
		}

	}



	public void GoToTeach()
	{
		ReplayTeach.SetValue(true);
		StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Teach"));
		GetComponent<ClickButtonSound>().Play();
		GetComponent<UIEvent>().SaveData();
	}
	public void GoToStartStory()
	{
		ReplayStartStory.SetValue(true);
		StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("StartStory"));
		GetComponent<ClickButtonSound>().Play();
		GetComponent<UIEvent>().SaveData();
	}
/***************************************************************************/
	public void CloseUFO_Teach()
	{
		chossui.planet_Teach04 = false;
		UFOMenu_Button.SetActive (true);
		planet_Teach_5.SetActive (false);
		planet_Teach_menu.SetActive (false);
        achievement_1.SetValue(true);
	}
	IEnumerator planet_Teach_04(float time)
	{
		yield return new WaitForSeconds(time);
		planet_Teach_5.SetActive (true);
		planet_Teach_4.SetActive (false);
	}
	IEnumerator Button_UFOMenu(float time)
	{
		yield return new WaitForSeconds(time);
		UFOMenu_Button.SetActive (true);

	}

/***************************************************************************/
}