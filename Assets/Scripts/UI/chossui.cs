﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class chossui : MonoBehaviour
{

	public GameObject planet_Teach_1;
	public GameObject planet_Teach_2;
	public GameObject planet_Teach_3;
	public Button Bt_planet;
	public Button Bt_UFO;
	public Text Coins;
	public Text Stars;
	public static int Teach_Coins = 0;
	public static int Teach_Stars = 0;
	public static bool planet_Teach02 = false;
	public static bool planet_Teach03 = false;
	public static bool planet_Teach04 = false;

    public IntVariable coin;
    public IntVariable totalStars;

	void Start()
	{
		Time.timeScale = 1;
	}
	void Update()
	{
		Coins.text = coin.Value.ToString ();
		Stars.text = totalStars.Value.ToString ();
		if (planet_Teach02 == true && planet_Teach03 == false) 
		{
			planet_Teach_1.SetActive (false);
			planet_Teach_2.SetActive (true);
		}
		if (planet_Teach02 == true && planet_Teach03 == true) 
		{
			planet_Teach_1.SetActive (false);
			planet_Teach_2.SetActive (false);
			planet_Teach_3.SetActive (true);
			Bt_planet.interactable = false;
			Bt_UFO.interactable = true;
			planet_Teach02 = false;
			planet_Teach04 = true;
		}
		if (planet_Teach02 == false) 
		{
			planet_Teach_1.SetActive (true);
			planet_Teach_2.SetActive (false);
		}
	}

	public void UfoMenuopen()
    {
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("UfoMenu"));
	}

	public void OpenTeach()
    {
		if (planet_Teach02 == true) 
		{
            StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("SelectLevel"));
		}

		if (planet_Teach02 == false) 
		{
            StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Teach"));
		}
	}


}
