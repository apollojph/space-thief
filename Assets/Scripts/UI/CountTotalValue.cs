﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountTotalValue : MonoBehaviour
{
    public IntVariable totalVariable;

    public IntVariable[] variables;

	void Start ()
    {
        totalVariable.Value = 0;

        for (int i = 0; i < variables.Length; i++)
        {
            totalVariable.ApplyChange(variables[i].Value);
        }

        Object.FindObjectOfType<DataSystem>().SaveData();
    }
}
