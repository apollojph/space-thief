﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Story : MonoBehaviour
{
	
	public int page = 1;
	public GameObject[] storyPages;
	public GameObject nextPageButton;

	void Start ()
	{
		if (page == 1)
		{
			storyPages [0].SetActive (true);
			StartCoroutine (GOPage2 (7.0f));
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.S))
        {
            Skip();
        }
	}
    public void Skip()
    {
        page = 7;

        for (int i = 0; i < storyPages.Length; i++)
        {
            storyPages[i].SetActive(false);
        }
        storyPages[6].SetActive(true);
        GetComponent<UIEvent>().GoTeachMenu();
       
        nextPageButton.SetActive(true);
    }
	public void StoryChage()
	{
		page += 1;
		if (page == 2)
		{
			storyPages [0].SetActive (false);
			storyPages [1].SetActive (true);
		}

		if (page == 3)
		{
			storyPages [1].SetActive (false);
			storyPages [2].SetActive (true);
		}
		if (page == 4)
		{
			storyPages [2].SetActive (false);
			storyPages [3].SetActive (true);
		}
		if (page == 5)
		{
			storyPages [3].SetActive (false);
			storyPages [4].SetActive (true);
		}
		if (page == 6)
		{
			storyPages [4].SetActive (false);
			storyPages [5].SetActive (true);
		}
		if (page == 7)
		{
			storyPages [5].SetActive (false);
			storyPages [6].SetActive (true);
		}

	}
		
	public IEnumerator buttonshow(float show)
	{
		yield return new WaitForSeconds(show);
		nextPageButton.SetActive (true);
	
	}
	public IEnumerator GOPage2(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [0].SetActive (false);
		storyPages [1].SetActive (true);
		StartCoroutine (GOPage3 (8.27f));
	}

	public IEnumerator GOPage3(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [1].SetActive (false);
		storyPages [2].SetActive (true);
		StartCoroutine (GOPage4 (4.17f));
	}
	public IEnumerator GOPage4(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [2].SetActive (false);
		storyPages [3].SetActive (true);
		StartCoroutine (GOPage5 (6.12f));
	}
	public IEnumerator GOPage5(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [3].SetActive (false);
		storyPages [4].SetActive (true);
		StartCoroutine (GOPage6 (7.3f));
	}
	public IEnumerator GOPage6(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [4].SetActive (false);
		storyPages [5].SetActive (true);
		StartCoroutine (GOPage7 (7.3f));
	}
	public IEnumerator GOPage7(float show)
	{
		yield return new WaitForSeconds(show);
		page += 1;
		storyPages [5].SetActive (false);
		storyPages [6].SetActive (true);
		StartCoroutine (buttonshow (3.8f));
	}

}
