﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIEvent : MonoBehaviour 
{
    public GameObject loadingUI;
    public Image loadingBar;

    public IntVariable unlockLevel;

    public BoolVariable unlockAllNormalLevel;

	public BoolVariable ReplayTeach;
	public BoolVariable ReplayStartStory;

    public IEnumerator DisplayLoadingScreen(string sceneName)
    {
        loadingUI.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);

        while (!async.isDone)
        {
            loadingBar.transform.localScale = new Vector2(async.progress, loadingBar.transform.localScale.y);

            yield return null;
        }
    }

    public void SaveData()
    {
        Object.FindObjectOfType<DataSystem>().SaveData();
    }

    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoTeachMenu()
    {
		if(GetComponent<Story>().page >=7 && ReplayStartStory.Value == false)
		{
            GetComponent<Story>().page = 1;
			ReplayStartStory.SetValue (true);
			StartCoroutine(DisplayLoadingScreen("TeachMenu"));
			SaveData();
		}
		if(GetComponent<Story>().page >= 7 && ReplayStartStory.Value == true)
		{
            GetComponent<Story>().page = 1;
			StartCoroutine(DisplayLoadingScreen("MainMenu"));
			SaveData();
		}
		GetComponent<ClickButtonSound>().Play();

    }
	public void GoToStartStory()
	{
		StartCoroutine(DisplayLoadingScreen("StartStory"));
		GetComponent<ClickButtonSound>().Play();
		SaveData();
	}

    public void GoToTeachMenu()
    {
		if (ReplayTeach.Value == false) 
		{
			StartCoroutine (DisplayLoadingScreen ("TeachMenu"));
			chossui.planet_Teach02 = true;
		}
		if (ReplayTeach.Value == true) 
		{
			StartCoroutine(DisplayLoadingScreen("MainMenu"));
		}
		GetComponent<ClickButtonSound>().Play();
		SaveData();
    }

    public void GoToMainMenu()
    {
        if (chossui.planet_Teach02 == true)
        {
            StartCoroutine(DisplayLoadingScreen("TeachMenu"));
            chossui.planet_Teach03 = true;
        }

        if (chossui.planet_Teach02 == false)
        {
            StartCoroutine(DisplayLoadingScreen("MainMenu"));
        }
		GetComponent<ClickButtonSound>().Play();	
        SaveData();
    }

    public void GoToSelectLevel()
    {
        StartCoroutine(DisplayLoadingScreen("SelectLevel"));
		GetComponent<ClickButtonSound>().Play();
		SaveData();
    }

    public void GoToLevel1()
    {
        StartCoroutine(DisplayLoadingScreen("Level1"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel2()
    {
        StartCoroutine(DisplayLoadingScreen("Level2"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel3()
    {
        StartCoroutine(DisplayLoadingScreen("Level3"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel4()
    {
        StartCoroutine(DisplayLoadingScreen("Level4"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel5()
    {
        StartCoroutine(DisplayLoadingScreen("Level5"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel1Hard()
    {
        StartCoroutine(DisplayLoadingScreen("Level1Hard"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel2Hard()
    {
        StartCoroutine(DisplayLoadingScreen("Level2Hard"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel3Hard()
    {
        StartCoroutine(DisplayLoadingScreen("Level3Hard"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel4Hard()
    {
        StartCoroutine(DisplayLoadingScreen("Level4Hard"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void GoToLevel5Hard()
    {
        StartCoroutine(DisplayLoadingScreen("Level5Hard"));
        GetComponent<ClickButtonSound>().Play();
        SaveData();
    }

    public void AccessLevel1()
    {
        if (unlockLevel.Value < 1 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(1);
            print("getlevel" + unlockLevel.Value);
            SaveData();
        }
    }

    public void AccessLevel2()
    {
        if (unlockLevel.Value < 2 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(2);
            print("getlevel" + unlockLevel.Value);
            SaveData();
        }
    }
    public void AccessLevel3()
    {
        if (unlockLevel.Value < 3 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(3);
            print("getlevel" + unlockLevel.Value);
            SaveData();
        }
    }
    public void AccessLevel4()
    {
        if (unlockLevel.Value < 4 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(4);
            print("getlevel" + unlockLevel.Value);
            SaveData();
        }
    }
    public void AccessLevel5()
    {
        if (unlockLevel.Value < 5 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(5);
            print("getlevel" + unlockLevel.Value);
            SaveData();
        }
    }
    public void AccessAllNormalLevel()
    {
        if (unlockLevel.Value <= 5 && unlockAllNormalLevel.Value == false)
        {
            unlockLevel.SetValue(5);
            unlockAllNormalLevel.SetValue(true);

            Object.FindObjectOfType<DataSystem>().gameData.unlockAllNormalLevelValue = true;

            print("getlevel" + unlockLevel.Value);

            SaveData();
        }
    }
}
