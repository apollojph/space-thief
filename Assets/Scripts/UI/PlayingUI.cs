﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingUI : UISystem
{
    public PlayerController player;
    public GameObject turnFastOnButton;
    public GameObject turnFastOffButton;
    public GameObject pauseButton;
    public GameObject continueButton;

    public override void Initialize()
    {
        Hide();
    }

    public void TurnOnFast()
    {
        player.speed = 3.0f;
        turnFastOnButton.SetActive(false);
        turnFastOffButton.SetActive(true);
    }
    public void TurnOffFast()
    {
        player.speed = 1.5f;
        turnFastOnButton.SetActive(true);
        turnFastOffButton.SetActive(false);
    }

    public void Pause()
    {
        Time.timeScale = 0;
        Object.FindObjectOfType<EmojiSystem>().Hide();
        continueButton.SetActive(true);
        GetComponent<ClickButtonSound>().Play();
    }

    public void Continue()
    {
        Time.timeScale = 1;
        continueButton.SetActive(false);
        Object.FindObjectOfType<EmojiSystem>().Show();
        GetComponent<ClickButtonSound>().Play();
    }
}
