﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickButtonSound : MonoBehaviour
{
    public AudioSource buttonSound;
    public AudioClip buttonSource;

    public void Play()
    {
        buttonSound.PlayOneShot(buttonSource);
    }
}
