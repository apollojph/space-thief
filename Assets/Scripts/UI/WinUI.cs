﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinUI : UISystem 
{
    public IntVariable coinsVariable;
    public IntVariable starVariable;
    public IntVariable crystalVariable;
    public GameObject Star01;
    public GameObject Star02;
    public GameObject Star03;
    public Text gainCoinsText;
    public Text gainCrystalText;

    public override void Initialize()
    {
        Hide();
        HideAllStar();
    }

    public void HideAllStar()
    {
        Star01.SetActive(false);
        Star02.SetActive(false);
        Star03.SetActive(false);
    }

    public void ShowStar(int num)
    {
        switch(num)
        {
            case 1:
                Star01.SetActive(true);
                break;
            case 2:
                Star01.SetActive(true);
                Star02.SetActive(true);
                break;
            case 3:
                Star01.SetActive(true);
                Star02.SetActive(true);
                Star03.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void StoreStarValue(int value)
    {
        if(starVariable.Value < value)
        {
            starVariable.SetValue(value);
        }
    }

    public void StoreCrystalValue(int value)
    {
        if (crystalVariable.Value < value)
        {
            crystalVariable.SetValue(value);
        }
    }

    public void SetGainCoinsText(string content)
    {
        gainCoinsText.text = content;
    }

    public void SetCrystalText(string content)
    {
        gainCrystalText.text = content;
    }

    public void AddCoins(int value)
    {
        int currentCoin = coinsVariable.Value;
        int nextCoin = coinsVariable.Value + value;

        if(currentCoin < nextCoin)
        {
            coinsVariable.ApplyChange(value);
        }
    }
}
