﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour
{

	public GameObject P1;
	public GameObject P2;
	public GameObject Stopmenu;
	public GameObject storedvaluemenu;
	public GameObject diamondtocoinmenu;
	public GameObject Buyfeedmenu;

    public IntVariable coins;
    public IntVariable diamonds;
    public IntVariable food;
    public IntVariable totalStars;

	public Text coinsText;
	public Text diamondsText;
	public Text foodsText;
	public Text starsText;

	void Start ()
	{
		Time.timeScale = 1;
	}

	void Update()
	{
		coinsText.text = coins.Value.ToString();
		diamondsText.text = diamonds.Value.ToString();
		foodsText.text = food.Value.ToString();
		starsText.text = totalStars.Value.ToString("D1");

		if (Input.GetKeyUp(KeyCode.A)) 
		{
			Time.timeScale = 0;
		}

		if (Input.GetKeyUp(KeyCode.S)) 
		{
			Time.timeScale = 1;
		}

		if (Input.GetKeyUp(KeyCode.B)) 
		{

			P1.SetActive (true);
			P2.SetActive (true);
		}

		if (Input.GetKeyUp(KeyCode.N)) 
		{

			P1.SetActive (false);
		}

		if (Input.GetKeyUp(KeyCode.M)) 
		{

			P2.SetActive (false);
		}
		if (Input.GetKeyUp(KeyCode.KeypadPlus)) 
		{

			coins.Value += 1000;
		}
	}

	public void storedvalueclose()
	{
		storedvaluemenu.SetActive (false);
	}

	public void storedvalueopen()
	{
		storedvaluemenu.SetActive (true);
	}

	public void diamondtocoinclose()
	{
		diamondtocoinmenu.SetActive (false);
	}

	public void diamondtocoinopen()
	{
		diamondtocoinmenu.SetActive (true);
	}

	public void Buyfeedmenuclose()
	{
		Buyfeedmenu.SetActive (false);
	}

	public void Buyfeedmenuopen()
	{
		Buyfeedmenu.SetActive (true);
	}

	public void UfoMenuopen()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("UfoMenu"));
	}

	public void OpenChoose1()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("SelectLevel"));
	}

	public void OpenTeach()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Teach"));
	}

	public void PurchaseDiamonds(int value)
	{
        diamonds.ApplyChange(value);

	}

	public void PurchaseCoins(int value)
	{
        coins.ApplyChange(value);
	}

	public void CostDiamonds(int value)
	{	
		int currentDiamond = diamonds.Value;
		int nextDiamond = diamonds.Value - value;

		if(currentDiamond > nextDiamond && nextDiamond > -1)
		{
			UfoMenu.eat_check = true;
			diamonds.ApplyChange(-value);
			GetComponent<UIEvent>().SaveData();
		}

	}
	public void CostCoins(int value)
	{
		
		int currentCoin = coins.Value;
		int nextCoin = coins.Value - value;

		if(currentCoin > nextCoin && nextCoin > -1)
		{
			UfoMenu.eat_check = true;
			coins.ApplyChange(-value);
			GetComponent<UIEvent>().SaveData();
		}

	}
	public void PurchaseFoods(int value)
	{
		int currentFood = food.Value;
		int nextFood = food.Value + value;

		if(UfoMenu.eat_check == true && currentFood < nextFood && nextFood <= 50)
		{
			//UfoMenu.eat_check = false;
			food.ApplyChange(value);
			GetComponent<UIEvent>().SaveData();
		}

	}

	/*public void PurchaseFoods(int value)
	{
		if (UfoMenu.eat_check == true && GameManager.Instance.Food<50 ) 
		{
			//UfoMenu.eat_check = false;
			GameManager.Instance.Food += value;
		}
	}*/
		

	public void playplanet1()
	{

		//Application.LoadLevel ("Level1");
	}
}
