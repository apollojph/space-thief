﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chosseui01_hard : MonoBehaviour
{

	public GameObject Planet01;
	public GameObject ChooseUI;
	public GameObject ChooseUImap;
	public GameObject ChooseUI_Button;
	public GameObject ChooseUIloop;
	public GameObject ChooseHard_Button;
	public GameObject ChooseHardStaus;
	public GameObject bt_101;
	public GameObject bt_102;
	public GameObject bt_103;
	public GameObject bt_104;
	public GameObject bt_105;
	public GameObject Standby_101;
	public GameObject Standby_102;
	public GameObject Standby_103;
	public GameObject Standby_104;
	public GameObject Standby_105;
	public GameObject Associate1_Shrink;
	public GameObject Associate2_Shrink;
	public GameObject Associate3_Shrink;
	public GameObject Associate4_Shrink;
	public GameObject Associate5_Shrink;
	public GameObject Associate1_Enlarge;
	public GameObject Associate2_Enlarge;
	public GameObject Associate3_Enlarge;
	public GameObject Associate4_Enlarge;
	public GameObject Associate5_Enlarge;
	public GameObject c1_Shrink;
	public GameObject c2_Shrink;
	public GameObject c3_Shrink;
	public GameObject c4_Shrink;
	public GameObject c5_Shrink;
	public GameObject c1_Enlarge;
	public GameObject c2_Enlarge;
	public GameObject c3_Enlarge;
	public GameObject c4_Enlarge;
	public GameObject c5_Enlarge;
	public GameObject confirm_1_Shrink;
	public GameObject confirm_2_Shrink;
	public GameObject confirm_3_Shrink;
	public GameObject confirm_4_Shrink;
	public GameObject confirm_5_Shrink;
	public GameObject confirm_1_Enlarge;
	public GameObject confirm_2_Enlarge;
	public GameObject confirm_3_Enlarge;
	public GameObject confirm_4_Enlarge;
	public GameObject confirm_5_Enlarge;
	public GameObject confirm_1;
	public GameObject confirm_2;
	public GameObject confirm_3;
	public GameObject confirm_4;
	public GameObject confirm_5;
	public GameObject Levelline_1;
	public GameObject Levelline_2;
	public GameObject Levelline_3;
	public GameObject Levelline_4;
	public GameObject Levelline_5;
	public GameObject LevelDot_1;
	public GameObject LevelDot_2;
	public GameObject LevelDot_3;
	public GameObject LevelDot_4;
	public GameObject LevelDot_5;
	public GameObject LevelSimple_1;
	public GameObject LevelSimple_2;
	public GameObject LevelSimple_3;
	public GameObject LevelSimple_4;
	public GameObject LevelSimple_5;
	public GameObject LevelBt_1;
	public GameObject LevelBt_2;
	public GameObject LevelBt_3;
	public GameObject LevelBt_4;
	public GameObject LevelBt_5;
	public GameObject Close_level101;
	public GameObject Close_level102;
	public GameObject Close_level103;
	public GameObject Close_level104;
	public GameObject Close_level105;
    public GameObject Star_01_Right;
    public GameObject Star_02_Right;
    public GameObject Star_03_Right;
    public GameObject Star_01_Left;
    public GameObject Star_02_Left;
    public GameObject Star_03_Left;
    public Button Bt_01;
	public Button Bt_02;
	public Button Bt_03;
	public Button Bt_04;
	public Button Bt_05;

    public IntVariable[] levelStars;

    void Start ()
    {
		StartCoroutine(Playchoose(0.7f));
		ChooseUI.SetActive (true);

	}

    /*******************************************************************/

    public void ShowStar(string side, int value)
    {
        GameObject star01 = null;
        GameObject star02 = null;
        GameObject star03 = null;

        switch (side)
        {
            case "Right":
                star01 = Star_01_Right;
                star02 = Star_02_Right;
                star03 = Star_03_Right;
                break;
            case "Left":
                star01 = Star_01_Left;
                star02 = Star_02_Left;
                star03 = Star_03_Left;
                break;
        }

        switch (value)
        {
            case 1:
                star01.SetActive(true);
                star02.SetActive(false);
                star03.SetActive(false);
                break;
            case 2:
                star01.SetActive(true);
                star02.SetActive(true);
                star03.SetActive(false);
                break;
            case 3:
                star01.SetActive(true);
                star02.SetActive(true);
                star03.SetActive(true);
                break;
            default:
                star01.SetActive(false);
                star02.SetActive(false);
                star03.SetActive(false);
                break;
        }
    }

    public void HideStar(string side)
    {
        switch (side)
        {
            case "Right":
                Star_01_Right.SetActive(false);
                Star_02_Right.SetActive(false);
                Star_03_Right.SetActive(false);
                break;
            case "Left":
                Star_01_Left.SetActive(false);
                Star_02_Left.SetActive(false);
                Star_03_Left.SetActive(false);
                break;
        }
    }

    /*************************************************************************/
    public void PlayLevel1_hard()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Level1Hard"));
	}
	public void PlayLevel2_hard()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Level2Hard"));
	}
	public void PlayLevel3_hard()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Level3Hard"));
	}
	public void playLevel4_hard()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Level4Hard"));
	}
	public void playLevel5_hard()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("Level5Hard"));
	}

	public void ClooseChoose1()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("MainMenu"));
	}
	public void ChooseSimple()
	{
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("SelectLevel"));
	}



	/*************************************************************************************/

	IEnumerator Playchoose(float time)
	{
		yield return new WaitForSeconds(time);
		Planet01.SetActive (true);
		ChooseUIloop.SetActive (true);
		ChooseUImap.SetActive (true);
		ChooseUI_Button.SetActive (true);
		ChooseHard_Button.SetActive (true);
		ChooseHardStaus.SetActive (true);
		Bt_01.enabled=true;
		Bt_02.enabled=true;
		Bt_03.enabled=true;
		Bt_04.enabled=true;
		Bt_05.enabled=true;
		bt_101.SetActive (true);
		bt_102.SetActive (true);
		bt_103.SetActive (true);
		bt_104.SetActive (true);
		bt_105.SetActive (true);
		Standby_101.SetActive (true);
		Standby_102.SetActive (true);
		Standby_103.SetActive (true);
		Standby_104.SetActive (true);
		Standby_105.SetActive (true);
		Associate1_Enlarge.SetActive (false);
		Associate2_Enlarge.SetActive (false);
		Associate3_Enlarge.SetActive (false);
		Associate4_Enlarge.SetActive (false);
		Associate5_Enlarge.SetActive (false);
		c1_Enlarge.SetActive (false);
		c2_Enlarge.SetActive (false);
		c3_Enlarge.SetActive (false);
		c4_Enlarge.SetActive (false);
		c5_Enlarge.SetActive (false);


	}



	/*******************************************************************/

	public void ClickChoose101(){
		Bt_01.enabled=false;
		bt_102.SetActive (false);
		bt_103.SetActive (false);
		bt_104.SetActive (false);
		bt_105.SetActive (false);
		Standby_101.SetActive (false);
		Standby_102.SetActive (false);
		Standby_103.SetActive (false);
		Standby_104.SetActive (false);
		Standby_105.SetActive (false);
		Associate1_Shrink.SetActive (true);
		Associate2_Shrink.SetActive (true);
		Associate3_Shrink.SetActive (true);
		Associate4_Shrink.SetActive (true);
		Associate5_Shrink.SetActive (true);
		c1_Shrink.SetActive (true);
		c2_Shrink.SetActive (true);
		c3_Shrink.SetActive (true);
		c4_Shrink.SetActive (true);
		c5_Shrink.SetActive (true);
		confirm_1_Enlarge.SetActive (true);
		StartCoroutine(choose101(0.6f));
	}
	IEnumerator choose101(float time)
	{
		yield return new WaitForSeconds(time);
		Associate1_Shrink.SetActive (false);
		Associate2_Shrink.SetActive (false);
		Associate3_Shrink.SetActive (false);
		Associate4_Shrink.SetActive (false);
		Associate5_Shrink.SetActive (false);
		c1_Shrink.SetActive (false);
		c2_Shrink.SetActive (false);
		c3_Shrink.SetActive (false);
		c4_Shrink.SetActive (false);
		c5_Shrink.SetActive (false);
		confirm_1_Enlarge.SetActive (false);
		confirm_1.SetActive (true);
		Levelline_1.SetActive (true);
		LevelDot_1.SetActive (true);
		LevelSimple_1.SetActive (true);
		LevelBt_1.SetActive (true);
		Close_level101.SetActive (true);

        ShowStar("Right", levelStars[0].Value);
    }
	public void CloseLevel101()
    {

		confirm_1.SetActive (false);
		Levelline_1.SetActive (false);
		LevelDot_1.SetActive (false);
		LevelSimple_1.SetActive (false);
		LevelBt_1.SetActive (false);
		Close_level101.SetActive (false);
		confirm_1_Shrink.SetActive (true);

        HideStar("Right");

        StartCoroutine(Openchoose(0.6f));
	}

	/**************************************************************************/

	public void ClickChoose102()
    {
		bt_101.SetActive (false);
		Bt_02.enabled=false;
		bt_103.SetActive (false);
		bt_104.SetActive (false);
		bt_105.SetActive (false);
		Standby_101.SetActive (false);
		Standby_102.SetActive (false);
		Standby_103.SetActive (false);
		Standby_104.SetActive (false);
		Standby_105.SetActive (false);
		Associate1_Shrink.SetActive (true);
		Associate2_Shrink.SetActive (true);
		Associate3_Shrink.SetActive (true);
		Associate4_Shrink.SetActive (true);
		Associate5_Shrink.SetActive (true);
		c1_Shrink.SetActive (true);
		c2_Shrink.SetActive (true);
		c3_Shrink.SetActive (true);
		c4_Shrink.SetActive (true);
		c5_Shrink.SetActive (true);
		confirm_2_Enlarge.SetActive (true);
		StartCoroutine(choose102(0.6f));
	}
	IEnumerator choose102(float time)
	{
		yield return new WaitForSeconds(time);
		Associate1_Shrink.SetActive (false);
		Associate2_Shrink.SetActive (false);
		Associate3_Shrink.SetActive (false);
		Associate4_Shrink.SetActive (false);
		Associate5_Shrink.SetActive (false);
		c1_Shrink.SetActive (false);
		c2_Shrink.SetActive (false);
		c3_Shrink.SetActive (false);
		c4_Shrink.SetActive (false);
		c5_Shrink.SetActive (false);
		confirm_2_Enlarge.SetActive (false);
		confirm_2.SetActive (true);
		Levelline_2.SetActive (true);
		LevelDot_2.SetActive (true);
		LevelSimple_2.SetActive (true);
		LevelBt_2.SetActive (true);
		Close_level102.SetActive (true);

        ShowStar("Left", levelStars[1].Value);
    }
	public void CloseLevel102(){

		confirm_2.SetActive (false);
		Levelline_2.SetActive (false);
		LevelDot_2.SetActive (false);
		LevelSimple_2.SetActive (false);
		LevelBt_2.SetActive (false);
		Close_level102.SetActive (false);
		confirm_2_Shrink.SetActive (true);

        HideStar("Left");

        StartCoroutine(Openchoose(0.6f));
    }

	/**************************************************************************/

	public void ClickChoose103(){
		bt_101.SetActive (false);
		bt_102.SetActive (false);
		Bt_03.enabled=false;
		bt_104.SetActive (false);
		bt_105.SetActive (false);
		Standby_101.SetActive (false);
		Standby_102.SetActive (false);
		Standby_103.SetActive (false);
		Standby_104.SetActive (false);
		Standby_105.SetActive (false);
		Associate1_Shrink.SetActive (true);
		Associate2_Shrink.SetActive (true);
		Associate3_Shrink.SetActive (true);
		Associate4_Shrink.SetActive (true);
		Associate5_Shrink.SetActive (true);
		c1_Shrink.SetActive (true);
		c2_Shrink.SetActive (true);
		c3_Shrink.SetActive (true);
		c4_Shrink.SetActive (true);
		c5_Shrink.SetActive (true);
		confirm_3_Enlarge.SetActive (true);
		StartCoroutine(choose103(0.6f));
	}
	IEnumerator choose103(float time)
	{
		yield return new WaitForSeconds(time);
		Associate1_Shrink.SetActive (false);
		Associate2_Shrink.SetActive (false);
		Associate3_Shrink.SetActive (false);
		Associate4_Shrink.SetActive (false);
		Associate5_Shrink.SetActive (false);
		c1_Shrink.SetActive (false);
		c2_Shrink.SetActive (false);
		c3_Shrink.SetActive (false);
		c4_Shrink.SetActive (false);
		c5_Shrink.SetActive (false);
		confirm_3_Enlarge.SetActive (false);
		confirm_3.SetActive (true);
		Levelline_3.SetActive (true);
		LevelDot_3.SetActive (true);
		LevelSimple_3.SetActive (true);
		LevelBt_3.SetActive (true);
		Close_level103.SetActive (true);

        ShowStar("Right", levelStars[2].Value);

    }
	public void CloseLevel103()
    {

		confirm_3.SetActive (false);
		Levelline_3.SetActive (false);
		LevelDot_3.SetActive (false);
		LevelSimple_3.SetActive (false);
		LevelBt_3.SetActive (false);
		Close_level103.SetActive (false);
		confirm_3_Shrink.SetActive (true);

        HideStar("Right");

        StartCoroutine(Openchoose(0.6f));
    }

	/*******************************************************************************/

	public void ClickChoose104()
    {
		bt_101.SetActive (false);
		bt_102.SetActive (false);
		bt_103.SetActive (false);
		Bt_04.enabled=false;
		bt_105.SetActive (false);
		Standby_101.SetActive (false);
		Standby_102.SetActive (false);
		Standby_103.SetActive (false);
		Standby_104.SetActive (false);
		Standby_105.SetActive (false);
		Associate1_Shrink.SetActive (true);
		Associate2_Shrink.SetActive (true);
		Associate3_Shrink.SetActive (true);
		Associate4_Shrink.SetActive (true);
		Associate5_Shrink.SetActive (true);
		c1_Shrink.SetActive (true);
		c2_Shrink.SetActive (true);
		c3_Shrink.SetActive (true);
		c4_Shrink.SetActive (true);
		c5_Shrink.SetActive (true);
		confirm_4_Enlarge.SetActive (true);
		StartCoroutine(choose104(0.6f));
	}
	IEnumerator choose104(float time)
	{
		yield return new WaitForSeconds(time);
		Associate1_Shrink.SetActive (false);
		Associate2_Shrink.SetActive (false);
		Associate3_Shrink.SetActive (false);
		Associate4_Shrink.SetActive (false);
		Associate5_Shrink.SetActive (false);
		c1_Shrink.SetActive (false);
		c2_Shrink.SetActive (false);
		c3_Shrink.SetActive (false);
		c4_Shrink.SetActive (false);
		c5_Shrink.SetActive (false);
		confirm_4_Enlarge.SetActive (false);
		confirm_4.SetActive (true);
		Levelline_4.SetActive (true);
		LevelDot_4.SetActive (true);
		LevelSimple_4.SetActive (true);
		LevelBt_4.SetActive (true);
		Close_level104.SetActive (true);

        ShowStar("Right", levelStars[3].Value);
    }
	public void CloseLevel104(){

		confirm_4.SetActive (false);
		Levelline_4.SetActive (false);
		LevelDot_4.SetActive (false);
		LevelSimple_4.SetActive (false);
		LevelBt_4.SetActive (false);
		Close_level104.SetActive (false);
		confirm_4_Shrink.SetActive (true);

        HideStar("Right");

        StartCoroutine(Openchoose(0.6f));

        
    }

	/*******************************************************************************/

	public void ClickChoose105(){
		bt_101.SetActive (false);
		bt_102.SetActive (false);
		bt_103.SetActive (false);
		bt_104.SetActive (false);
		Bt_05.enabled=false;
		Standby_101.SetActive (false);
		Standby_102.SetActive (false);
		Standby_103.SetActive (false);
		Standby_104.SetActive (false);
		Standby_105.SetActive (false);
		Associate1_Shrink.SetActive (true);
		Associate2_Shrink.SetActive (true);
		Associate3_Shrink.SetActive (true);
		Associate4_Shrink.SetActive (true);
		Associate5_Shrink.SetActive (true);
		c1_Shrink.SetActive (true);
		c2_Shrink.SetActive (true);
		c3_Shrink.SetActive (true);
		c4_Shrink.SetActive (true);
		c5_Shrink.SetActive (true);
		confirm_5_Enlarge.SetActive (true);
		StartCoroutine(choose105(0.6f));
	}
	IEnumerator choose105(float time)
	{
		yield return new WaitForSeconds(time);
		Associate1_Shrink.SetActive (false);
		Associate2_Shrink.SetActive (false);
		Associate3_Shrink.SetActive (false);
		Associate4_Shrink.SetActive (false);
		Associate5_Shrink.SetActive (false);
		c1_Shrink.SetActive (false);
		c2_Shrink.SetActive (false);
		c3_Shrink.SetActive (false);
		c4_Shrink.SetActive (false);
		c5_Shrink.SetActive (false);
		confirm_5_Enlarge.SetActive (false);
		confirm_5.SetActive (true);
		Levelline_5.SetActive (true);
		LevelDot_5.SetActive (true);
		LevelSimple_5.SetActive (true);
		LevelBt_5.SetActive (true);
		Close_level105.SetActive (true);

        ShowStar("Left", levelStars[4].Value);
    }
	public void CloseLevel105()
    {

		confirm_5.SetActive (false);
		Levelline_5.SetActive (false);
		LevelDot_5.SetActive (false);
		LevelSimple_5.SetActive (false);
		LevelBt_5.SetActive (false);
		Close_level105.SetActive (false);
		confirm_5_Shrink.SetActive (true);

        HideStar("Left");

        StartCoroutine(Openchoose(0.6f));
    }

	/*******************************************************************************/

	IEnumerator Openchoose(float time)
	{
		yield return new WaitForSeconds(time);
		confirm_1_Shrink.SetActive (false);
		confirm_2_Shrink.SetActive (false);
		confirm_3_Shrink.SetActive (false);
		confirm_4_Shrink.SetActive (false);
		confirm_5_Shrink.SetActive (false);
		Associate1_Enlarge.SetActive (true);
		Associate2_Enlarge.SetActive (true);
		Associate3_Enlarge.SetActive (true);
		Associate4_Enlarge.SetActive (true);
		Associate5_Enlarge.SetActive (true);
		c1_Enlarge.SetActive (true);
		c2_Enlarge.SetActive (true);
		c3_Enlarge.SetActive (true);
		c4_Enlarge.SetActive (true);
		c5_Enlarge.SetActive (true);
		StartCoroutine(Playchoose(0.4f));
	}
}
