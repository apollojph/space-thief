﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Achievement : MonoBehaviour 
{

	public GameObject[] Achieevement_List_4;
	public GameObject[] Achieevement_List_7;

    public IntVariable food;
    public BoolVariable achievement_1;
    public BoolVariable achievement_2;
    public BoolVariable achievement_3;
    public BoolVariable achievement_4;
    public BoolVariable achievement_5;
    public BoolVariable achievement_6;
    public BoolVariable achievement_7;
    public BoolVariable achievement_8_1;
    public BoolVariable achievement_8_2;
    public BoolVariable achievement_8_3;
    public BoolVariable achievementTaken_1;
    public BoolVariable achievementTaken_2;
    public BoolVariable achievementTaken_3;
    public BoolVariable achievementTaken_4;
    public BoolVariable achievementTaken_5;
    public BoolVariable achievementTaken_6;
    public BoolVariable achievementTaken_7;
    public BoolVariable achievementTaken_8_1;
    public BoolVariable achievementTaken_8_2;
    public BoolVariable achievementTaken_8_3;
    public BoolVariable unlockAllNormalLevel;
    public IntVariable totalStars;

	public GameObject achievementList_8_1;
	public GameObject achievementList_8_2;
	public GameObject achievementList_8_3;

	public Button button_1;
	public Button button_2;
	public Button button_3;
	public Button button_4;
	public Button button_5;
	public Button button_6;
	public Button button_7;
	public Button button_8_1;
	public Button button_8_2;
	public Button button_8_3;

    public Sprite taken;

	void Start ()
	{
		button_1.interactable = false;
		button_2.interactable = false;
		button_3.interactable = false;
		button_4.interactable = false;
		button_5.interactable = false;
		button_6.interactable = false;
		button_7.interactable = false;
		button_8_1.interactable = false;
		button_8_2.interactable = false;
		button_8_3.interactable = false;

        Initialized();
	}

	private void Update()
	{
        if (food.Value >= 10 && achievementTaken_8_1.Value == false)
        {
            button_8_1.interactable = true;
            print("Can Take");
        }

        if (food.Value >= 30 && achievementTaken_8_2.Value == false)
        {
            button_8_2.interactable = true;
        }

        if (food.Value >= 50 && achievementTaken_8_2.Value == false)
        {
            button_8_3.interactable = true;
        }
	}

	public void Initialized()
	{
        if (achievement_1.Value == true && achievementTaken_1.Value == false)
        {
            button_1.interactable = true;
        }

        if (unlockAllNormalLevel.Value && achievementTaken_2.Value == false)
        {
            button_2.interactable = true;
        }

        if (totalStars.Value >= 10 && achievementTaken_4.Value == false)
        {
            button_4.interactable = true;
        }

        if (achievementTaken_8_1.Value && !achievementTaken_8_2.Value && !achievementTaken_8_3.Value)
        {
            achievementList_8_1.SetActive(false);
            achievementList_8_2.SetActive(true);
        }

        if (achievementTaken_8_1.Value && achievementTaken_8_2.Value && !achievementTaken_8_3.Value)
        {
            achievementList_8_1.SetActive(false);
            achievementList_8_2.SetActive(false);
            achievementList_8_3.SetActive(true);
        }

        if (achievementTaken_8_1.Value && achievementTaken_8_2.Value && achievementTaken_8_3.Value)
        {
            achievementList_8_1.SetActive(false);
            achievementList_8_2.SetActive(false);
            achievementList_8_3.SetActive(true);
        }

        if (achievementTaken_1.Value)
        {
            ChangeImageToTaken(button_1);
        }

        if (achievementTaken_2.Value)
        {
            ChangeImageToTaken(button_2);
        }

        if (achievementTaken_3.Value)
        {
            ChangeImageToTaken(button_3);
        }

        if (achievementTaken_4.Value)
        {
            ChangeImageToTaken(button_4);
        }

        if (achievementTaken_5.Value)
        {
            ChangeImageToTaken(button_5);
        }

        if (achievementTaken_6.Value)
        {
            ChangeImageToTaken(button_6);
        }

        if (achievementTaken_7.Value)
        {
            ChangeImageToTaken(button_7);
        }

        if (achievementTaken_8_1.Value)
        {
            ChangeImageToTaken(button_8_1);
        }

        if (achievementTaken_8_2.Value)
        {
            ChangeImageToTaken(button_8_2);
        }

        if (achievementTaken_8_3.Value)
        {
            ChangeImageToTaken(button_8_3);
        }
	}

	public void Achievement_8_2()
	{
        achievementList_8_1.SetActive (false);
        achievementList_8_2.SetActive (true);
	}

	public void Achievement_8_3()
	{
        achievementList_8_2.SetActive (false);
        achievementList_8_3.SetActive (true);
	}

    public void AchievementTaken(BoolVariable achievementTaken)
    {
        achievementTaken.SetValue(true);
    }

    public void ChangeImageToTaken(Button button)
    {
        button.GetComponent<Image>().sprite = taken;
        button.interactable = false;
    }
}
