﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnTeachMode : Turn
{
    public bool turnOneshot = true;
    public bool turn = false;
    public bool turnOn = false;

    private void Awake()
    {
        player = GetComponent<PlayerController>();
    }

    public override void Perform()
    {
        Time.timeScale = 1;
        if (turnOneshot == false)
        {
            if (player.direction == Vector3.back)
            {
                player.direction = Vector3.left;
                player.SetAnimation("Left");
                turnOneshot = true;
            }
            else if (player.direction == Vector3.right || player.direction == Vector3.up)
            {
                player.direction = Vector3.back;
                player.SetAnimation("Back");
                turnOneshot = true;
            }
            else if (player.direction == Vector3.down || player.direction == Vector3.left)
            {
                player.direction = Vector3.forward;
                player.SetAnimation("Forward");
                turnOneshot = true;
            }
            else if (player.direction == Vector3.forward)
            {
                player.direction = Vector3.right;
                player.SetAnimation("Right");
                turnOneshot = true;
            }
        }
        else if (turn == true)
        {
            if (player.direction == Vector3.back)
            {
                player.direction = Vector3.left;
                player.SetAnimation("Left");
            }
            else if (player.direction == Vector3.right || player.direction == Vector3.up)
            {
                player.direction = Vector3.back;
                player.SetAnimation("Back");
            }
            else if (player.direction == Vector3.down || player.direction == Vector3.left)
            {
                player.direction = Vector3.forward;
                player.SetAnimation("Forward");
            }
            else if (player.direction == Vector3.forward)
            {
                player.direction = Vector3.right;
                player.SetAnimation("Right");
            }
        }
    }
}
