﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicStick : MonoBehaviour, IShoot
{
    [SerializeField]
    private float spawnTime = 1f;

    [SerializeField]
    private int spawnAmount = 1;

    [SerializeField]
    private GameObject magicProjectile;

    private Vector3 shootDirection = Vector3.forward;

    [SerializeField]
    private List<Vector3> usedShootDirection = new List<Vector3> ();


    private void Start()
    {
        //Every X second is to shoot projectile
        InvokeRepeating("Shoot", spawnTime, spawnTime);
    }

    public void Shoot()
    {
        for(int i = 0; i < spawnAmount; i++)
        {
            GetShootDirection();
            GameObject projectileInstance = Instantiate(magicProjectile, transform.position, Quaternion.identity);
            projectileInstance.GetComponent<Projectile>().direction = shootDirection;
            projectileInstance.GetComponent<Projectile>().startPosition = projectileInstance.transform.position;
        }

        //Empty Shoot Direction's Record
        usedShootDirection = new List<Vector3>();
    }

    private void GetShootDirection()
    {

        //Get Random Direction

        int selector = Random.Range(1, 5);

        switch(selector)
        {
            case 1:
                shootDirection = Vector3.forward;
                break;
            case 2:    
				shootDirection = Vector3.back;
                break;
            case 3:
				shootDirection = Vector3.right;
                break;
            case 4:
				shootDirection = Vector3.left;
                break;
        }

        
        //Check the direction is not use.
        for(int i = 0; i < usedShootDirection.Count; i++)
        {
            if(shootDirection == usedShootDirection[i])
            {
                GetShootDirection();
                break;
            }
        }

        print("Access");

        usedShootDirection.Add(shootDirection);
    }
}
