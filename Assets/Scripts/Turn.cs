﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn : MonoBehaviour
{
    protected PlayerController player;

    private void Awake()
    {
        player = GetComponent<PlayerController> ();
    }

    public virtual void Perform()
    {
        if(player.state == PlayerController.State.Moving || player.state == PlayerController.State.HitObstacle || player.state == PlayerController.State.NearCrystal || player.state == PlayerController.State.NearUFO)
        {
            if(player.direction == Vector3.back)
            {
                player.direction = Vector3.left;
                player.SetAnimation("Left");
            }
            else if (player.direction == Vector3.right || player.direction == Vector3.up)
            {
                player.direction = Vector3.back;
                player.SetAnimation("Back");
            }
            else if (player.direction == Vector3.down || player.direction == Vector3.left)
            {
                player.direction = Vector3.forward;
                player.SetAnimation("Forward");
            }
            else if (player.direction == Vector3.forward)
            {
                player.direction = Vector3.right;
                player.SetAnimation("Right");
            }

            //GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        }
    }
}
