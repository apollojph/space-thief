﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

	[System.Serializable]
	public class LevelStar
	{
		public int[] value = new int[5];
	}

	public LevelStar[] levelStar = new LevelStar[2];

	private int coin = 0;
	private int diamond = 0;
	private int food = 0;
	private int star = 0;
	private float timer = 0;

	public static bool Achieevement_1 = false;
	public static bool Achieevement_2 = false;
	public static bool Achieevement_3 = false;
	public static bool Achieevement_4 = false;
	public static bool Achieevement_5 = false;
	public static bool Achieevement_6 = false;
	public static bool Achieevement_7 = false;
	public static bool Achieevement_8_1 = true;
	public static bool Achieevement_8_2 = false;
	public static bool Achieevement_8_3 = false;

	private static GameManager _instance;

	private static object _lock = new object();

	public static GameManager Instance
	{
		get
		{
			lock( _lock )
			{
				if( _instance == null )
				{
					if( _instance == null )
					{
						GameObject singleton = new GameObject();

						_instance = singleton.AddComponent<GameManager>();
						singleton.name = "[Singleton] " + typeof(GameManager).ToString();

						DontDestroyOnLoad( singleton );

						Debug.Log( "[Singleton] An instance of " + typeof(GameManager) +
							" is needed in the scene, so '" + singleton +
							"' was created with DontDestroyOnLoad.");
					}
					else
					{
						Debug.Log( "[Singleton] Using instance already created: " +
							_instance.gameObject.name );
					}
				}
			}

			return _instance;
		}
	}

	public int Coin
	{
		get 
		{
			return coin;
		}
		set 
		{
			if (coin >= 0) 
			{
				coin = value;
			}
		}
	}

	public int Diamond
	{
		get 
		{
			return diamond;
		}
		set 
		{
			if (diamond >= 0) 
			{
				diamond = value;
			}
		}
	}

	public int Food
	{
		get 
		{
			return food;
		}
		set 
		{
			if (food >= 0) 
			{
				food = value;
			}
		}
	}

	public int Star
	{
		get 
		{
			return star;
		}
	}

	public float Timer
	{
		get 
		{
			return timer;
		}
		set 
		{
			if (timer >= 0) 
			{
				timer = value;
			}
		}
	}


	private void Awake()
	{
		if (_instance != null)
		{
			Debug.LogErrorFormat(this.gameObject, "Multiple instances of {0} is not allow", GetType().Name);
			return;
		}

		_instance = this;
		GameObject.DontDestroyOnLoad(this.gameObject);
	}

	public void CountStar()
	{
		star = 0;

		for (int i = 0; i < levelStar.Length; i++) 
		{
			for (int j = 0; j < levelStar[i].value.Length; j++) 
			{
				star += levelStar [i].value [j];
			}
		}
	}
}
