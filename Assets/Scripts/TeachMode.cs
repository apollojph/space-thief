﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeachMode : MonoBehaviour
{

    public Button turnButton;
    public GameObject GameTeach01;
    public GameObject GameTeach02;
    public GameObject GameTeach03;
    public GameObject GameTeach04;
    public GameObject GameTeach05;
    public GameObject GameTeach06;
    public GameObject GameTeach07;
    public GameObject GameTeach08;
    public GameObject GameTeach09;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().tag == "Ufo")
        {
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach03.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach05.SetActive(false);
            GameTeach06.SetActive(false);
            GameTeach07.SetActive(false);
            GameTeach08.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Collider>().tag == "Teach01")
        {
            print("Teach01");
            GameTeach02.SetActive(false);
            GameTeach01.SetActive(true);
            GameTeach03.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach05.SetActive(false);
            turnButton.interactable = false;
        }
        if (other.GetComponent<Collider>().tag == "Teach02")
        {
            print("Teach02");
            Time.timeScale = 0;
            GetComponent<TurnTeachMode> ().turnOneshot = false;
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(true);
            GameTeach03.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach05.SetActive(false);
            turnButton.interactable = true;
        }

        if (other.GetComponent<Collider>().tag == "Teach03")
        {
            print("Teach03");
            GetComponent<TurnTeachMode>().turnOneshot = false;
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach03.SetActive(true);
            GameTeach04.SetActive(false);
            GameTeach05.SetActive(false);
            turnButton.interactable = false;
        }
        if (other.GetComponent<Collider>().tag == "Teach04")
        {
            print("Teach04");
            Time.timeScale = 0;
            GetComponent<TurnTeachMode>().turnOneshot = false;
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach04.SetActive(true);
            GameTeach03.SetActive(false);
            GameTeach05.SetActive(false);
            turnButton.interactable = true;
        }
        if (other.GetComponent<Collider>().tag == "Teach05")
        {
            print("Teach05");
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach03.SetActive(false);
            GameTeach05.SetActive(true);
            turnButton.interactable = false;
        }
        if (other.GetComponent<Collider>().tag == "Teach06")
        {
            print("Teach06");
            GetComponent<PlayerController>().speed = 0;
            GetComponent<TurnTeachMode>().turnOneshot = false;
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach03.SetActive(false);
            GameTeach05.SetActive(false);
            GameTeach09.SetActive(true);
            turnButton.interactable = false;
            StartCoroutine(Teach10(2.0f));
        }
        if (other.GetComponent<Collider>().tag == "Teach07")
        {
            print("Teach07");
            Time.timeScale = 0;
            GetComponent<TurnTeachMode>().turnOneshot = false;
            GameTeach01.SetActive(false);
            GameTeach02.SetActive(false);
            GameTeach04.SetActive(false);
            GameTeach03.SetActive(false);
            GameTeach05.SetActive(false);
            GameTeach09.SetActive(false);
            GameTeach07.SetActive(true);
            turnButton.interactable = true;
        }
        if (other.GetComponent<Collider>().tag == "Teach08")
        {
            print("Teach08");
            GameTeach07.SetActive(false);
            turnButton.interactable = false;
        }
        if (other.GetComponent<Collider>().tag == "Teach00")
        {
            print("Teach00");
            GetComponent<TurnTeachMode>().turn = true;
            GameTeach06.SetActive(false);
            GameTeach07.SetActive(false);
            GameTeach08.SetActive(true);
            turnButton.interactable = false;
        }
    }

    IEnumerator Teach10(float time)
    {
        yield return new WaitForSeconds(time);
        GameTeach09.SetActive(false);
        GameTeach06.SetActive(true);
        StartCoroutine(Teach11(3.0f));
    }

    IEnumerator Teach11(float time)
    {
        yield return new WaitForSeconds(time);
        
        if (GetComponent<TurnTeachMode>().turnOn == true)
        {
            GetComponent<PlayerController>().speed = 3.0f;
        }
        if (GetComponent<TurnTeachMode>().turnOn == false)
        {
            GetComponent<PlayerController>().speed = 1.5f;
        }
    }
}
