﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class SpawnPoints
{
    public GameObject enemy;
    public List<Transform> destination;
}

public class EnemySystem : MonoBehaviour 
{
    [SerializeField]
    private float startTime = 0;

    [SerializeField]
    private List<SpawnPoints> spawnPoints = new List<SpawnPoints>();

    private bool isSpawn = false;

    private void Update()
    {

        //Spawn Enemy

        if (Time.time - startTime >= 0f && !isSpawn)
        {
            for (int i = 0; i < spawnPoints.Count; i++)
            {
                spawnPoints[i].enemy.GetComponent<EnemyAI>().startPosition = spawnPoints[i].enemy.transform.position;

                for (int j = 0; j < spawnPoints[i].destination.Count; j ++)
                {
                    spawnPoints[i].enemy.GetComponent<EnemyAI>().endPosition.Add(spawnPoints[i].destination[j].position);
                }

                spawnPoints[i].enemy.GetComponent<EnemyAI>().endPosition.Add(spawnPoints[i].enemy.GetComponent<EnemyAI>().startPosition);
                spawnPoints[i].enemy.GetComponent<EnemyAI>().SetDestination(0);
            }

            isSpawn = true;
        }
    }


}
