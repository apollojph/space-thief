﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayTeaching : MonoBehaviour {

	public Button Bt_return;
	public Button Bt_nextLevel;

	void Update ()
	{
		if (chossui.planet_Teach02 == true && chossui.planet_Teach03 == false) 
		{
			Bt_return.interactable = false;
			Bt_nextLevel.interactable = false;
		}
	}
}
