﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class SurvivalTimer : MonoBehaviour {


	public Text timerLabel; //#2
	private string timerText; 
	private string diffHour; 
	private string diffMin; 
	private string diffSec; 
	private float temp; 
	private float time; 

	void Start()
	{

	}

	void Update () 
	{  


		DateTime endtime = DateTime.Now.AddMinutes(5);
		DateTime time = DateTime.Now;

		TimeSpan MySpan = endtime.Subtract (time);

		timerText = string.Format("{0:D2}:{1:D2}:{2:D2}", MySpan.Hours, MySpan.Minutes, MySpan.Seconds);

		timerLabel.text = timerText;

		timerLabel= GetComponent<Text>();
		StartCoroutine (timewait());



	}
	IEnumerator timewait()
	{
		
		yield return new WaitForSeconds (1);
	}



}



 
