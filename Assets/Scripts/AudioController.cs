﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioController : MonoBehaviour 
{
	public AudioMixer masterMixer;

//	public GameObject musicslider;
//	public GameObject soundslider;
//	public Slider musicslider;
//	public Slider soundslider;
//
//	void Awake()
//	{
//
//	}

	public void SetSoundEffect (float sfx)
	{
		masterMixer.SetFloat ("SoundEffectVol", sfx);
	}
	public void SetMusic (float music)
	{
		masterMixer.SetFloat ("MusicVol", music);
	}
}
