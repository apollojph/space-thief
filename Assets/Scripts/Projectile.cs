﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour 
{
    public float speed = 5;

    public Vector3 direction = Vector3.forward;

    public Vector3 startPosition = Vector3.zero;

	private void Update () 
	{
		Move();

		//print("Dis: " + Vector3.Distance(transform.position, startPosition));

		if(Vector3.Distance(startPosition, transform.position) >= 2f)
		{
			Destroy(gameObject);
		}
	}

    private void Move()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }
}
