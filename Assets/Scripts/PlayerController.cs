﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public enum State { Idle, Moving, HitObstacle, NearCrystal, NearUFO, Fail, Win };
    public State state;

    public int requiredCrystalNumber = 1;
    public GameObject UFO;
    public GameObject playermesh;
    public AudioSource crystalAudioSource;
    public AudioSource lightingAudioSource;
    public AudioSource fireAudioSource;
    public AudioSource backgroundAudioSource;
    public Animator ufoEndAnimator;
    public Animator environmentAnimator;
    public WinUI winUI;
    public FailUI failUI;
    public PlayingUI playingUI;

    public float speed = 6.0F;
    public float gravity = 20.0F;
    public Vector3 direction = Vector3.forward;
    public float emojiChangeDelay = 0.5f;

    private Vector3 moveDirection = Vector3.zero;
    private EmojiSystem emojiSystem;
    private CharacterController controller;
    private int currentCrystalNumber = 0;
    private int previousCrystalNumber = 0;
    private bool isDelayShowEmoji = false;
    private bool isExit = true;
	private Animator animator;

	[SerializeField]
	private int coinBonus = 0;

	[SerializeField]
	private float timeBonus = 0;

	[SerializeField]
	private bool canAvoidPolice = false;

    private Renderer rend;

	public BoolVariable ReplayTeach;
	public IntVariable food;
    public IntVariable clothNumber;

    public Texture[] clothTextures;

    public Renderer playerStartInUFORenderer;
    public Renderer playerEndInUFORenderer;

    private void Awake()
    {
        emojiSystem = GetComponent<EmojiSystem>();
        controller = GetComponent<CharacterController>();
		animator = playermesh.GetComponent<Animator>();
        rend = playermesh.transform.GetChild(0).GetComponent<Renderer>();
    }

    private void Start()
    {
        failUI.Initialize();
        winUI.Initialize();
        playingUI.Initialize();

        speed = 0;
        playermesh.SetActive(false);
        ufoEndAnimator.speed = 0;
        environmentAnimator.speed = 0;
        Time.timeScale = 1;
        UFO.SetActive(false);

        SetBouns();
        SetCloth();

        GetComponent<TimelineController>().Play(0);

        StartCoroutine(PlayStart(3.7f));
    }

    private void SetBouns()
    {
        if (food.Value >= 10)
        {
            timeBonus = -10;
        }

        if (food.Value >= 30)
        {
            coinBonus = 250;
        }

        if (food.Value >= 50)
        {
            canAvoidPolice = true;
        }

        if (clothNumber.Value == 1)
        {
            coinBonus += 250;
        }
    }

    private void SetCloth()
    {
        if (clothNumber.Value == 0)
        {
            rend.material.mainTexture = clothTextures[0];
            playerStartInUFORenderer.material.mainTexture = clothTextures[0];
            playerEndInUFORenderer.material.mainTexture = clothTextures[0];
        }
        else if (clothNumber.Value == 1)
        {
            rend.material.mainTexture = clothTextures[1];
            playerStartInUFORenderer.material.mainTexture = clothTextures[1];
            playerEndInUFORenderer.material.mainTexture = clothTextures[1];
        }
        else if (clothNumber.Value == 2)
        {
            rend.material.mainTexture = clothTextures[2];
            playerStartInUFORenderer.material.mainTexture = clothTextures[2];
            playerEndInUFORenderer.material.mainTexture = clothTextures[2];
        }
        else
        {
            rend.material.mainTexture = clothTextures[3];
            playerStartInUFORenderer.material.mainTexture = clothTextures[3];
            playerEndInUFORenderer.material.mainTexture = clothTextures[3];
        }
    }

    private void Update()
    {
        #if UNITY_EDITOR

        if (Input.GetKeyUp(KeyCode.A))
        {

            Time.timeScale = 0;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {

            Time.timeScale = 1;
        }

        if (Input.GetKeyUp(KeyCode.L))
        {
            Application.LoadLevel("Choose1");
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            StartCoroutine(Win(0.1f));
        }

        #endif

        
    }

    private void FixedUpdate()
    {
        if (state == State.Moving || state == State.HitObstacle || state == State.NearCrystal || state == State.NearUFO)
        {
            FrontDetect();

            if (controller.isGrounded)
            {
                moveDirection = direction;
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection.x *= speed;
                moveDirection.y *= speed;
                moveDirection.z *= speed;
            }

            moveDirection.y -= gravity * Time.deltaTime;
            controller.Move(moveDirection * Time.deltaTime);
        }
    }

    private void FrontDetect()
    {
        Ray frontRay = new Ray(transform.position, transform.TransformDirection(direction));
        RaycastHit frontHit;
        int layerMask = 1 << 8;

        Debug.DrawLine(transform.position, transform.TransformDirection(direction) * 100f, Color.red);

        if (Physics.Raycast(frontRay, out frontHit, 0.43f, layerMask))
        {
            print("Hit Name: " + frontHit.collider.name);

            if (frontHit.collider.tag == "Obstacle")
            {
                print("Wall");

                GetComponent<Turn>().Perform();

                ChangeState(State.HitObstacle);
                emojiSystem.ChangeEmoji(4);

                BeforeChangeMovingState();
            }
            else if (frontHit.collider.tag == "Slope")
            {
                Vector3 side = Vector3.zero;

                int yRotation = (int)(frontHit.transform.rotation.eulerAngles.y);

                switch (yRotation)
                {
                    case 90:
                    case 270:
                        side = Vector3.right;
                        break;
                    case 0:
                    case 180:
                        side = Vector3.forward;
                        break;
                }

                if (Vector3.Dot(direction, side) == 1f || Vector3.Dot(direction, side) == -1f)
                {
                    float angle = GetSlopeAngle();

                    if(angle <= 0f)
                    {
                        GetComponent<Turn>().Perform();
                        ChangeState(State.HitObstacle);
                        emojiSystem.ChangeEmoji(4);

                        BeforeChangeMovingState();
                    }
                }
            }
        }

    }
	
    private void BeforeChangeMovingState()
    {
        emojiSystem.isChanging = true;
        emojiSystem.StopAllCoroutines();
        StopCoroutine(ReturnToMovingState());
        isDelayShowEmoji = false;
        StartCoroutine(ReturnToMovingState());
    }

    private float GetSlopeAngle()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.down));

        if (Physics.Raycast(ray, out hit, 1000))
        {
            float slopeAngle = Vector3.Angle(transform.up, hit.normal);

			print("Slope Angle: " + slopeAngle);

            return slopeAngle;
        }
        
        return 0;
    }

    public void SetAnimation(string animationName)
    {
        //Set all animation to false
		animator.SetBool("Left", false);
		animator.SetBool("Right", false);
		animator.SetBool("Forward", false);
		animator.SetBool("Back", false);

        //Turn on the wanted animation
		animator.SetBool(animationName, true);
    }

    private IEnumerator ReturnToMovingState()
    {
        if(!isDelayShowEmoji)
        {
            isDelayShowEmoji = true;
            yield return new WaitForSeconds(emojiChangeDelay);
            isDelayShowEmoji = false;
            emojiSystem.isChanging = false;
            ChangeState(State.Moving);
        }
    }

    private void ChangeState(State newState)
    {
        state = newState;
    }

    private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Collider>().tag == "Crystal")
		{
			print("Crystal+Ufo");

            crystalAudioSource.Play();
            other.gameObject.SetActive(false);

            if (currentCrystalNumber < requiredCrystalNumber)
            {
                previousCrystalNumber = currentCrystalNumber;

                currentCrystalNumber++;

                if (currentCrystalNumber == requiredCrystalNumber)
                {
                    UFO.SetActive(true);
                }
            }
		}

		if (other.GetComponent<Collider>().tag == "Ufo")
		{
			print("Ufo");

            StartCoroutine(Win(2.5f));
		}

		if (other.GetComponent<Collider>().tag == "Lighting")
		{
			print("Lighting");
			lightingAudioSource.Play();
		}

		if (other.GetComponent<Collider>().tag == "Fire")
		{
			print("Fire");
			fireAudioSource.Play();
		}

        if (other.GetComponent<Collider>().tag == "ExclamationPoint")
        {
            if(currentCrystalNumber < requiredCrystalNumber)
            {

                isExit = false;

                ChangeState(State.NearCrystal);
                emojiSystem.ChangeEmoji(3);
                emojiSystem.isChanging = true;
                emojiSystem.StopAllCoroutines();
            }
        }

        if (other.GetComponent<Collider>().tag == "Emoji_ufo")
        {
            if (currentCrystalNumber == requiredCrystalNumber)
            {
                print("Emoji_ufo=true");

                isExit = true;

                ChangeState(State.NearUFO);
                emojiSystem.ChangeEmoji(2);
                emojiSystem.isChanging = true;
                emojiSystem.StopAllCoroutines();
            }
                
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Collider>().tag == "ExclamationPoint")
        {
            if (currentCrystalNumber < requiredCrystalNumber && state != State.Fail)
            {
                if(previousCrystalNumber == currentCrystalNumber)
                {
                    ChangeState(State.NearCrystal);

                    emojiSystem.isChanging = true;
                    emojiSystem.StopAllCoroutines();

                    if (emojiSystem.emojiSources[3].activeSelf == false)
                    {
                        emojiSystem.ChangeEmoji(3);
                    }
                }
               
            }
            else
            {
                if(emojiSystem.emojiSources[3].activeSelf == true)
                {
                    emojiSystem.emojiSources[3].SetActive(false);
                    StopCoroutine(ReturnToMovingState());
                    isDelayShowEmoji = false;
                    StartCoroutine(ReturnToMovingState());
                }
            }
        }

        if (other.GetComponent<Collider>().tag == "Emoji_ufo")
        {
            if (currentCrystalNumber == requiredCrystalNumber && state != State.Win)
            {
                print("Emoji_ufo=true");

                if(state == State.Moving)
                {
                    ChangeState(State.NearUFO);

                    emojiSystem.isChanging = true;
                    emojiSystem.StopAllCoroutines();

                    if (emojiSystem.emojiSources[2].activeSelf == false)
                    {
                        emojiSystem.ChangeEmoji(2);
                    }
                }
            }

        }
    }

    private void OnTriggerExit(Collider other)
	{

		if (other.GetComponent<Collider>().tag == "Lighting")
		{
			print("noLighting");
			lightingAudioSource.Stop();

		}
        if (other.GetComponent<Collider>().tag == "Fire")
        {
            print("noFire");
            fireAudioSource.Stop();
        }

        if (other.GetComponent<Collider>().tag == "ExclamationPoint")
        {
            if (currentCrystalNumber < requiredCrystalNumber && !isExit)
            {
                print("ExclamationPoint=false");

                isExit = true;

                emojiSystem.emojiSources[3].SetActive(false);

                BeforeChangeMovingState();
            }
        }

        if (other.GetComponent<Collider>().tag == "Emoji_ufo")
        {
            if (currentCrystalNumber == requiredCrystalNumber && !isExit)
            {
                print("Emoji_ufo=false");

                isExit = true;

                emojiSystem.emojiSources[2].SetActive(false);

                BeforeChangeMovingState();
            }
        }

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        switch(hit.collider.tag)
        {
            case "Drown":
                failUI.SetFailStatus(0);
                StartCoroutine(Fail(0.1f));
                break;
            case "Fall":
                failUI.SetFailStatus(1);
                StartCoroutine(Fail(0.1f));
                break;
            case "Burn":
                failUI.SetFailStatus(2);
                StartCoroutine(Fail(0.1f));
                break;
            case "Electrocuted":
                failUI.SetFailStatus(3);
                StartCoroutine(Fail(0.1f));
                break;
            case "MagicBall":
                failUI.SetFailStatus(4);
                StartCoroutine(Fail(0f));
                break;
            case "Poison":
                failUI.SetFailStatus(5);
                StartCoroutine(Fail(0.1f));
                break;
            case "Police":
				if(canAvoidPolice)
				{
					Invoke("AvoidPoliceDisappear", 2.0f);
				}
				else
				{
					failUI.SetFailStatus(6);
                    StartCoroutine(Fail(0.3f));
				}
                break;
        }
    }

	private void AvoidPoliceDisappear()
	{
		canAvoidPolice = false;
	}

    private IEnumerator PlayStart(float time)
    {
        yield return new WaitForSeconds(time);
        playingUI.Show();
        playermesh.SetActive(true);
        speed = 1.5f;
        state = State.Moving;
    }

    private IEnumerator Win(float time)
    {
        emojiSystem.Hide();
        emojiSystem.StopAllCoroutines();
        emojiSystem.enabled = false;
        playermesh.SetActive(false);
        GetComponent<CharacterController>().enabled = false;
        speed = 0;
        ufoEndAnimator.speed = 1;
        GetComponent<TimelineController>().Play(1);
        ChangeState(State.Win);

        yield return new WaitForSeconds(time);

        environmentAnimator.speed = 1;
        HideOtherUIAndStopSound();

        if (chossui.planet_Teach02)
        {
            winUI.transform.GetChild(0).GetChild(1).GetComponent<Button>().interactable = false;
            winUI.transform.GetChild(0).GetChild(3).GetComponent<Button>().interactable = false;
        }

        int starValue = 0;
        string coinsText = "";
        int coinsValue = 0;

		float usedTime = CountTime.timer + timeBonus;

		if (usedTime <= 60)
        {
            starValue = 3;

            if (ReplayTeach.Value == false)
            {
                coinsValue = 750;
            }
            if (ReplayTeach.Value == true)
            {
                ReplayTeach.SetValue(false);
                coinsValue = 0;
            }
        }

		if (usedTime > 60 && usedTime < 90)
        {
            starValue = 2;

            if (ReplayTeach.Value == false)
            {
                coinsValue = 500;
            }
            if (ReplayTeach.Value == true)
            {
                ReplayTeach.SetValue(false);
                coinsValue = 0;
            }

        }

		if (usedTime > 90)
        {
            starValue = 1;

            if (ReplayTeach.Value == false)
            {
                coinsValue = 250;
            }

            if (ReplayTeach.Value == true)
            {
                ReplayTeach.SetValue(false);
                coinsValue = 0;
            }
        }

		coinsValue += coinBonus;

        coinsText = coinsValue.ToString();

        winUI.ShowStar(starValue);
        winUI.StoreStarValue(starValue);
        winUI.StoreCrystalValue(requiredCrystalNumber);
        winUI.SetGainCoinsText(coinsText);
        winUI.SetCrystalText(requiredCrystalNumber.ToString());
        winUI.AddCoins(coinsValue);

        chossui.Teach_Coins += coinsValue;

        winUI.Show();
        gameObject.SetActive(false);
    }

    private void HideOtherUIAndStopSound()
    {
        lightingAudioSource.Stop();
        backgroundAudioSource.Stop();
        playingUI.Hide();
    }

    private IEnumerator Fail(float time)
	{
        ChangeState(State.Fail);
        speed = 0f;
        emojiSystem.Hide();
        emojiSystem.StopAllCoroutines();
        emojiSystem.enabled = false;

        yield return new WaitForSeconds(time);

        HideOtherUIAndStopSound();
        UFO.SetActive(false);
        playermesh.SetActive(false);
        failUI.Show();
	}


}

