﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineController : MonoBehaviour
{

    public List<PlayableDirector> playableDirectors;

    /// <summary>
    /// Play In (index is 0), Out (index is 1) Timeline
    /// </summary>
    /// <param name="directorIndex"></param>

    public void Play(int directorIndex)
    {
        playableDirectors[directorIndex].Play();
    }
}
