﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToStartMenu : MonoBehaviour
{
	void Start ()
    {
        StartCoroutine(GetComponent<UIEvent>().DisplayLoadingScreen("StartMenu"));
	}
}
