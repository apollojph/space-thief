﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour 
{

    public Vector3 startPosition;
    public List<Vector3> endPosition;

    private Vector3 destination;
    private int currentDestinationIndex = 0;
    private NavMeshAgent navMeshAgent;


	void Awake () 
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
    public void SetDestination(int index)
    {
        destination = endPosition[index];
        navMeshAgent.destination = destination;
    }

	void Update () 
    {
        if (Vector3.Distance(transform.position, destination) < 0.5f)
        {
            if ( (currentDestinationIndex + 1) < endPosition.Count)
            {
                currentDestinationIndex++;
                SetDestination(currentDestinationIndex);
            }
            else
            {
                currentDestinationIndex = 0;
                SetDestination(currentDestinationIndex);
            }
        }
	}
}
