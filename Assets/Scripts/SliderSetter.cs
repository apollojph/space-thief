﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderSetter : MonoBehaviour 
{
    
    public FloatVariable variable;

	private Slider slider;

	void Awake()
	{
		slider = GetComponent<Slider> ();
	}

    private void Update()
    {
        float previousValue = slider.value;

        if(previousValue != variable.Value)
        {
            slider.value = variable.Value;
        }
    }

    public void SaveVolume()
	{
        variable.SetValue(slider.value);
	}
}
