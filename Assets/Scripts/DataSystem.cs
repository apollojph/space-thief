﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataSystem : MonoBehaviour
{

    public IntVariable coin;
    public IntVariable diamond;
    public IntVariable food;
    public IntVariable unlockLevel;
    public BoolVariable unlockAllNormalLevel;
    public BoolVariable isShowedUnlockHardLevel;
	public BoolVariable ReplayTeach;
	public BoolVariable ReplayStartStory;
    public FloatVariable musicVolume;
    public FloatVariable soundEffectsVolume;
    public IntVariable[] crystals;
    public IntVariable[] levelStars;
    public IntVariable totalStars;
    public BoolVariable[] achievement;
    public BoolVariable[] achievementTaken;
    public BoolVariable isBuyBeard;
    public BoolVariable isBuyMalay;
    public IntVariable clothesNumber;
    public GameData gameData;

    private string gameDataProjectFilePath = "/StreamingAssets/data.json";

    private void Awake()
    {
        LoadData();
    }

    public void SaveData()
    {
        gameData.coinValue = coin.Value;

        gameData.diamondValue = diamond.Value;

        if (gameData.foodValue < food.Value)
        {
            gameData.foodValue = food.Value;
        }

        if (gameData.unlockLevelValue < unlockLevel.Value)
        {
            gameData.unlockLevelValue = unlockLevel.Value;
        }

        gameData.unlockAllNormalLevelValue = unlockAllNormalLevel.Value;

        gameData.ReplayTeachValue = ReplayTeach.Value;

        gameData.ReplayStartStoryValue =  ReplayStartStory.Value;

        if (gameData.musicVolumeValue > musicVolume.Value)
        {
            gameData.musicVolumeValue = musicVolume.Value;
        }

        if (gameData.soundEffectsVolumeValue > soundEffectsVolume.Value)
        {
            gameData.soundEffectsVolumeValue = soundEffectsVolume.Value;
        }


        for (int i = 0; i < levelStars.Length; i++)
        {
            if (gameData.levelStarsValue[i] < levelStars[i].Value)
            {
                gameData.levelStarsValue[i] = levelStars[i].Value;
            }

        }

        for (int i = 0; i < crystals.Length; i++)
        {
            if (gameData.crystalsValue[i] < crystals[i].Value)
            {
                gameData.crystalsValue[i] = crystals[i].Value;
            }
        }

        for (int i = 0; i < achievement.Length; i++)
        {
            gameData.achievement[i] = achievement[i].Value;
        }

        for (int i = 0; i < achievementTaken.Length; i++)
        {
            gameData.achievementTaken[i] = achievementTaken[i].Value;
        }

        gameData.totalStars = totalStars.Value;
        gameData.isShowedUnlockHardLevel = isShowedUnlockHardLevel.Value;
        gameData.isBuyBeard = isBuyBeard.Value;
        gameData.isBuyMalay = isBuyMalay.Value;
        gameData.clothesNumber = clothesNumber.Value;

        string dataAsJson = JsonUtility.ToJson(gameData);

        #if UNITY_EDITOR
		string filePath = Application.persistentDataPath + gameDataProjectFilePath;

        #elif UNITY_IOS
		string filePath = Application.persistentDataPath + "/data.json";
 
        #elif UNITY_ANDROID
        string filePath = Application.persistentDataPath + "/data.json";
 
        #endif

        File.WriteAllText(filePath, dataAsJson);
    }

    public void LoadData()
    {

        #if UNITY_EDITOR
        string filePath = Application.persistentDataPath + gameDataProjectFilePath;

        #elif UNITY_IOS
		string filePath = Application.persistentDataPath + "/data.json";
 
        #elif UNITY_ANDROID
        string filePath = Application.persistentDataPath + "/data.json";

        #endif

        #if UNITY_EDITOR || UNITY_IOS

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData> (dataAsJson);
        }
        else
        {
            gameData = new GameData();
        }

        #elif UNITY_ANDROID

        StreamReader file = new StreamReader(Path.Combine(Application.persistentDataPath, "data.json"));
        string loadJson = file.ReadToEnd();
        file.Close();

        gameData = JsonUtility.FromJson<GameData> (loadJson);

        //WWW reader = new WWW (filePath);
        //while (!reader.isDone) { }
        //gameData = JsonUtility.FromJson<GameData> (reader.text);
        #endif

        coin.Value = gameData.coinValue;
        diamond.Value = gameData.diamondValue;
        food.Value = gameData.foodValue;
        unlockLevel.Value = gameData.unlockLevelValue;
		unlockAllNormalLevel.Value = gameData.unlockAllNormalLevelValue;
        ReplayTeach.Value = gameData.ReplayTeachValue;
        ReplayStartStory.Value = gameData.ReplayStartStoryValue;
        musicVolume.Value = gameData.musicVolumeValue;
        soundEffectsVolume.Value = gameData.soundEffectsVolumeValue;
        totalStars.Value = gameData.totalStars;
        isShowedUnlockHardLevel.Value = gameData.isShowedUnlockHardLevel;
        isBuyBeard.Value = gameData.isBuyBeard;
        isBuyMalay.Value = gameData.isBuyMalay;
        clothesNumber.Value = gameData.clothesNumber;

        for (int i = 0; i < levelStars.Length; i++)
        {
            levelStars[i].Value = gameData.levelStarsValue[i];
        }

        for (int i = 0; i < crystals.Length; i++)
        {
            crystals[i].Value = gameData.crystalsValue[i];
        }

        for (int i = 0; i < achievement.Length; i++)
        {
            achievement[i].Value = gameData.achievement[i];
        }

        for (int i = 0; i < achievementTaken.Length; i++)
        {
            achievementTaken[i].Value = gameData.achievementTaken[i];
        }
    }
}
